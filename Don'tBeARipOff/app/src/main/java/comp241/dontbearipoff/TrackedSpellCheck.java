package comp241.dontbearipoff;

import android.content.Context;
import android.view.textservice.SentenceSuggestionsInfo;
import android.view.textservice.SpellCheckerSession;
import android.view.textservice.SuggestionsInfo;
import android.view.textservice.TextInfo;
import android.view.textservice.TextServicesManager;

import java.util.Locale;

public class TrackedSpellCheck implements SpellCheckerSession.SpellCheckerSessionListener {
    private SpellCheckerSession mSession;

    private int mCorrectWords = 0;
    private int mTotalWords = 0;

    public TrackedSpellCheck(Context ctx) {
        final TextServicesManager tsm = (TextServicesManager) ctx.getSystemService(Context.TEXT_SERVICES_MANAGER_SERVICE);
        mSession = tsm.newSpellCheckerSession(null, Locale.ENGLISH, this, true);
    }

    public void check(String text) {
        mSession.getSuggestions(new TextInfo(text), 1);
    }

    public double getAccuracy() {
        if (mTotalWords > 0)
            return (mCorrectWords / (double) mTotalWords);
        return 0;
    }

    @Override
    public void onGetSuggestions(final SuggestionsInfo[] arg0) {
        if (arg0.length == 0) {
            ++mCorrectWords;
        }
        ++mTotalWords;
    }

    @Override
    public void onGetSentenceSuggestions(SentenceSuggestionsInfo[] arg0) {

    }

    public void reset() {
        mCorrectWords = 0;
        mTotalWords = 0;
    }
}
