package comp241.dontbearipoff;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hp.gagawa.java.Document;
import com.hp.gagawa.java.DocumentType;
import com.hp.gagawa.java.elements.P;

import org.opencv.core.Size;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import comp241.dontbearipoff.book.Book;
import comp241.dontbearipoff.book.BookTask;
import comp241.dontbearipoff.colorpicker.ColorPickerDialog;

public class NewBookActivity extends ActionBarActivity implements View.OnClickListener, BookTask.BookTaskListener, CategoryEditor.CategoryCallback {

    private Uri imageCaptureUri;

    private static final int PICK_FROM_CAMERA = 1;
    private static final int PICK_FROM_FILE = 2;
    private static final int MODIFY_IMAGES = 3;

    private EditText name, author, description;
    private ImageView cover;
    private SeekBar size;
    private TextView error;

    private Bitmap coverImage;
    private int coverColor = Color.WHITE;
    private int textColor = Color.BLACK;
    private int textSize = 20;
    private boolean hidden;

    private ArrayList<String> pages = new ArrayList<>();
    private ArrayList<Rect> regions = new ArrayList<>();
    private Rect dimensions = new Rect();
    private ArrayList<String> categories = new ArrayList<>();

    private int processed;
    private ArrayList<String> text = new ArrayList<>();

    private ProgressDialog progressDialog;
    private CategoryEditor categoryEditor;

    private int books = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_new_book);

        categoryEditor = new CategoryEditor(this, this);
        ViewGroup parent = (ViewGroup) findViewById(R.id.categories);

        categoryEditor.init(getLayoutInflater(), parent);
        categoryEditor.setVisible(true);

        cover = (ImageView) findViewById(R.id.imageCover);
        error = (TextView) findViewById(R.id.bookError);
        name = (EditText) findViewById(R.id.editBookName);

        name.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!hidden) drawCover(coverColor, textColor, textSize);
            }
        });

        size = (SeekBar) findViewById(R.id.seekTextSize);
        size.setProgress(textSize);
        size.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (progress < 1) seekBar.setProgress(1);

                drawCover(coverColor, textColor, seekBar.getProgress());
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });

        author = (EditText) findViewById(R.id.editBookAuthor);
        description = (EditText) findViewById(R.id.editBookDescription);

        description.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.editBookDescription) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);

                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }

                return false;
            }
        });

        Button createButton = (Button) findViewById(R.id.btnCreateBook);
        Button coverColorButton = (Button) findViewById(R.id.btnCoverColor);
        Button textColorButton = (Button) findViewById(R.id.btnTextColor);
        createButton.setOnClickListener(this);
        coverColorButton.setOnClickListener(this);
        textColorButton.setOnClickListener(this);

        drawCover(coverColor, textColor, textSize);

        final String[] items = new String[]{"From Camera", "From SD Card"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.select_dialog_item, items);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Select Image");
        builder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                if (item == 0) {
                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "tmp_avatar_" + String.valueOf(System.currentTimeMillis()) + ".jpg");
                    imageCaptureUri = Uri.fromFile(file);

                    try {
                        intent.putExtra(android.provider.MediaStore.EXTRA_OUTPUT, imageCaptureUri);
                        intent.putExtra("return-data", true);

                        startActivityForResult(intent, PICK_FROM_CAMERA);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    dialog.cancel();
                } else {
                    Intent intent = new Intent();

                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);

                    startActivityForResult(Intent.createChooser(intent, "Complete action using"), PICK_FROM_FILE);
                }
            }
        });

        final AlertDialog dialog = builder.create();

        findViewById(R.id.btnLoadCoverImage).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
            }
        });

        findViewById(R.id.btnModifyPages).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(NewBookActivity.this, PageEditActivity.class);

                intent.putStringArrayListExtra("pages", pages);
                startActivityForResult(intent, MODIFY_IMAGES);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_new_book, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_new_page: {
                Intent intent = new Intent(NewBookActivity.this, PageEditActivity.class);

                intent.putStringArrayListExtra("pages", pages);
                startActivityForResult(intent, MODIFY_IMAGES);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.btnCreateBook) {
            onCreateBook();
        } else if (view.getId() == R.id.btnCoverColor)
            new ColorPickerDialog(this, coverColor, new ColorPickerDialog.OnColorSelectedListener() {
                @Override
                public void onColorSelected(int color) {
                    drawCover(color, textColor, textSize);
                }
            }).show();
        else if (view.getId() == R.id.btnTextColor)
            new ColorPickerDialog(this, textColor, new ColorPickerDialog.OnColorSelectedListener() {
                @Override
                public void onColorSelected(int color) {
                    drawCover(coverColor, color, textSize);
                }
            }).show();
    }

    private void onCreateBook() {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        alert.setTitle("Error");

        if (name.getText().length() > 0 && author.getText().length() > 0) {
            if (!pages.isEmpty()) {
                if (!Storage.exists("Books", name.getText().toString() + ".epub")) {
                    processPages();

                    return;
                } else {
                    alert.setMessage("A book already exists with this name. Would you like to overwrite it?");
                    alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Storage.clearDirectory(getCacheDir() + "/" + name.getText().toString());
                            processPages();
                        }
                    });
                    alert.setNegativeButton("Cancel", null);

                    error.setVisibility(View.INVISIBLE);
                }
            } else {
                alert.setMessage("Please add at least one page to the book");
                alert.setPositiveButton("Ok", null);

                error.setText("* Please add at least one page to the book *");
                error.setVisibility(View.VISIBLE);
            }
        } else {
            alert.setMessage("Please fill out the book's name and author");
            alert.setPositiveButton("Ok", null);

            error.setText("* Please fill out the book's name and author *");
            error.setVisibility(View.VISIBLE);
        }

        alert.create().show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode != RESULT_OK) return;

        if (requestCode == PICK_FROM_CAMERA || requestCode == PICK_FROM_FILE) {
            Bitmap bitmap = null;
            String path = "";

            if (requestCode == PICK_FROM_FILE) {
                imageCaptureUri = data.getData();

                try {
                    bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageCaptureUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                path = imageCaptureUri.getPath();
                bitmap = BitmapFactory.decodeFile(path);
            }

            if (bitmap != null) {
                findViewById(R.id.customCover).setVisibility(View.GONE);

                if (requestCode == PICK_FROM_CAMERA) cover.setImageBitmap(BitmapUtils.decodeSampledBitmapFromFile(path, 300, 300));
                else cover.setImageBitmap(bitmap);

                coverImage = bitmap;
                hidden = true;
            }
        } else if (requestCode == MODIFY_IMAGES) {
            pages = data.getStringArrayListExtra("pages");
            regions = data.getParcelableArrayListExtra("regions");
            dimensions = data.getParcelableExtra("dimensions");

            ((TextView) findViewById(R.id.numPages)).setText("Number of Pages: " + pages.size());
        }
    }

    private void processPages() {
        new AlertDialog.Builder(this).setTitle("Select OCR Method").setMessage("Which OCR method would you like to use?").setPositiveButton("Fast OCR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                processPages(true);
            }
        }).setNegativeButton("Experimental OCR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                processPages(false);
            }
        }).create().show();
    }

    private void processPages(boolean method) {
        progressDialog = ProgressDialog.show(this, "Please Wait ...", "Saving Book ...", true, false);

        for (int i = 0; i < pages.size(); ++i) {
            String page = pages.get(i);
            Rect region = regions.get(i);

            PhotoProcessor photoProcessor = new PhotoProcessor(progressDialog.getContext());
            photoProcessor.setImage(BitmapFactory.decodeFile(page));

            Size imageDimensions = photoProcessor.getDimensions();

            // adjust region bounding box to ensure dimensions are accurate
            region.left = (int)((double)region.left / dimensions.right * imageDimensions.width);
            region.right = (int)((double)region.right / dimensions.right * imageDimensions.width);
            region.top = (int)((double)region.top / dimensions.right * imageDimensions.width);
            region.bottom = (int)((double)region.bottom / dimensions.right * imageDimensions.width);

            photoProcessor.setPageRegion(region);

            if (method) {
                photoProcessor.processFastText();
            } else {
                photoProcessor.processComplexText();
            }

            onProcessFinish(photoProcessor.getText());

            /* PhotoProcessor photoProcessor;

            photoProcessor = new PhotoProcessor(progressDialog.getContext());

            photoProcessor.setImage(BitmapFactory.decodeFile(page));
            photoProcessor.processText(); */

            //onProcessFinish(null);
        }
    }

    private void onProcessFinish(ArrayList<String> text) {
        Document document = new Document(DocumentType.HTMLFrameset);

        StringBuilder string = new StringBuilder();

        for (int i = 0; i < text.size(); i ++) {
            if (i != 0) string.append(" ");

            string.append(text.get(i));
        }

        document.body.appendChild(new P().appendText(string.toString()));

        //document.body.appendChild(new P().appendText(getResources().getString(R.string.ocr)));

        this.text.add(document.write());

        processed += 1;

        if (processed == pages.size()) createBook();
    }

    private void createBook() {
        String bookTitle = name.getText().toString();
        String bookAuthor = author.getText().toString();
        String bookDescription = description.getText().toString();
        books = 0;

        try {
            comp241.dontbearipoff.book.Book book = new Book(coverImage, getCacheDir() + "/" + bookTitle, Storage.getFilesDir("Books") + File.separator + bookTitle + ".epub", this);

            book.getMeta().setTitle(bookTitle);
            book.getMeta().setAuthor(bookAuthor);
            book.getMeta().setDescription(bookDescription);

            for (int i = 0; i < text.size(); i++) {
                InputStream inputStream = new ByteArrayInputStream(text.get(i).getBytes());

                book.addPage(inputStream, "Page " + (i + 1));
            }

            for (String category : categories) book.getMeta().set("metadata", category, "true");

            book.saveBook();

            Book pictureBook = new Book(coverImage, getCacheDir() + "/i" + bookTitle, Storage.getFilesDir("Books") + File.separator + bookTitle + ".iepub", this);

            pictureBook.getMeta().setTitle(bookTitle);
            pictureBook.getMeta().setAuthor(bookAuthor);
            pictureBook.getMeta().setDescription(bookDescription);

            for (String page : pages) pictureBook.addImagePage(page, "iPage" + pages.indexOf(page));

            pictureBook.saveBook();
        } catch (Exception e) {
            e.printStackTrace();
            progressDialog.dismiss();
            finish();
        }
    }

    private void drawCover(int coverColor, int textColor, int textSize) {
        this.coverColor = coverColor;
        this.textColor = textColor;
        this.textSize = textSize;

        float scale = getResources().getDisplayMetrics().density;

        Bitmap.Config bitmapConfig = Bitmap.Config.ARGB_8888;
        Bitmap bitmap = Bitmap.createBitmap(846, 1200, bitmapConfig);

        Canvas canvas = new Canvas(bitmap);

        Paint p = new Paint();
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setAntiAlias(true);
        p.setFilterBitmap(true);
        p.setDither(true);
        p.setColor(coverColor);

        canvas.drawRect(0, 0, bitmap.getWidth(), bitmap.getHeight(), p);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(textColor);
        paint.setTextSize((int) (textSize * scale));
        paint.setShadowLayer(1f, 0f, 1f, Color.rgb(61, 61, 61));

        Rect bounds = new Rect();
        String title = name.getText().toString();
        paint.getTextBounds(title, 0, title.length(), bounds);

        int y = 50;

        List<String> wrappedWords = new ArrayList<>();
        StringTokenizer st = new StringTokenizer(title);

        float spaceLeft = 746;
        float spaceWidth = paint.measureText(" ");
        String sentence = "";

        while (st.hasMoreTokens()) {
            String word = st.nextToken();

            if ((paint.measureText(word) + spaceWidth) > spaceLeft) {
                wrappedWords.add(sentence);

                sentence = word + " ";
                spaceLeft = 746 - paint.measureText(word);
            } else {
                sentence += word + " ";
                spaceLeft -= (paint.measureText(word) + spaceWidth);
            }
        }

        wrappedWords.add(sentence);

        for (String line : wrappedWords) {
            y += 5 + (int) (textSize * scale);

            canvas.drawText(line, bitmap.getWidth() / 2 - paint.measureText(line.trim()) / 2, y, paint);
        }

        coverImage = bitmap;

        cover.setImageBitmap(bitmap);
    }

    @Override
    public void onBookTaskProgress(BookTask.BookTaskType type, int progress) {
    }

    @Override
    public void onBookTaskComplete(BookTask.BookTaskType type, boolean success) {
        books += 1;

        if (books == 2) {
            progressDialog.dismiss();
            finish();
        }
    }

    @Override
    public void onAddCategory(Category category) {
        categories.add(category.getId());
    }

    @Override
    public void onRemoveCategory(Category category) {
        categories.remove(category.getId());
    }

    @Override
    public boolean hasCategory(Category category) {
        return categories.contains(category.getId());
    }
}
