package comp241.dontbearipoff;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

import comp241.dontbearipoff.book.BookTask;
import comp241.dontbearipoff.book.InvalidBookException;


public class BookViewActivity extends ActionBarActivity implements BookTask.BookTaskListener {

    public final String TAG = "BookView";
    public String currentPageName = "";
    private int pageNumber;

    private comp241.dontbearipoff.book.Book book;
    private comp241.dontbearipoff.book.Book.BookMeta meta;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_view);
        pageNumber = 1;
        /* Log.i(TAG, getIntent().getStringExtra("BookName"));
        openBook = new Book(Storage.getFilesDir("Books") + File.separator + getIntent().getStringExtra("BookName"), getCacheDir().getPath() + File.separator + getIntent().getStringExtra("BookName").replace(".epub", ""));
        openBook.openEpub();
        showPage();
        Log.i("Title: ", openBook.title);
        setTitle(openBook.title); */

        progressDialog = new ProgressDialog(this);

        progressDialog.setTitle("Please Wait ...");
        progressDialog.setMessage("Loading Book ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setMax(100);
        progressDialog.setCancelable(false);
        progressDialog.show();

        String bookName = File.separator + getIntent().getStringExtra("BookName");
        book = new comp241.dontbearipoff.book.Book(Storage.getFilesDir("Books") + bookName, getCacheDir() + bookName.replace(".epub", "").replace(".iepub", ""), this);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_book_view, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        switch (id) {
            case R.id.action_next:
                changePage(1);
                break;
            case R.id.action_previous:
                changePage(-1);
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    private void showPage() {
        WebView wV = (WebView) findViewById(R.id.web_view);

        String firstPagePath = meta.getPagePath(pageNumber);
        String html = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";

        BufferedReader input;

        try {
            input = new BufferedReader(new InputStreamReader(new FileInputStream(firstPagePath)));
            String nextLine;

            while ((nextLine = input.readLine()) != null) html += nextLine;
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
        } catch (NullPointerException e) {
            Log.e(firstPagePath, "= " + e.getMessage());
        }

        wV.loadDataWithBaseURL("file://" + book.getExtractLocation() + "/pages/", html, "text/html", "utf-8", null);
        //wV.loadData(html, "text/html; charset=UTF-8", "UTF-8");
    }

    private boolean changePage(int offset) {
        if (pageNumber + offset <= book.pageCount() && pageNumber + offset > 0) {

            pageNumber += offset;
            showPage();
            return true;
        }
        return false;
    }

    @Override
    public void onBookTaskProgress(BookTask.BookTaskType taskType, int progress) {
        progressDialog.setProgress(progress);

        if (progressDialog.getProgress() == progressDialog.getMax()) progressDialog.dismiss();
    }

    @Override
    public void onBookTaskComplete(BookTask.BookTaskType taskType, boolean success) {
        if (success) {
            try {
                meta = book.getMeta();
            } catch (InvalidBookException e) {
                e.printStackTrace();

                finish();
            }
        } else finish();

        if (book.isValid()) {
            setTitle(meta.getTitle());
            showPage();
        } else finish();
    }

}