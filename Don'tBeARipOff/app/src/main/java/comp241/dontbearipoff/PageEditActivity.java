package comp241.dontbearipoff;

import android.content.Intent;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class PageEditActivity extends ActionBarActivity {

    private static final int NEW_PAGE = 1;
    private static final int EDIT_PAGE = 2;

    private PagesFragment pagesFragment;
    private boolean open;

    private ArrayList<PagesFragment.ImageItem> pages = new ArrayList<>();
    private ArrayList<Rect> regions = new ArrayList<>();
    private Rect dimensions = new Rect();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle loadFrom = savedInstanceState != null && savedInstanceState.getStringArrayList("pages") != null ? savedInstanceState : getIntent().getExtras();

        for (String item : loadFrom.getStringArrayList("pages"))
            pages.add(new PagesFragment.ImageItem(item));

        pagesFragment = new PagesFragment();

        setContentView(R.layout.activity_page_edit);
        openMain();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        ArrayList<String> pagePaths = new ArrayList<>();

        for (PagesFragment.ImageItem item : pages) pagePaths.add(item.getImageLocation());

        outState.putStringArrayList("pages", pagePaths);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_page_edit, menu);

        return true;
    }

    @Override
    public void onBackPressed() {
        goBack();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.act_new_page:
                Intent newPage = new Intent(this, NewPageActivity.class);
                startActivityForResult(newPage, NEW_PAGE);

                return true;
            case R.id.action_finalize:
                goBack();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case NEW_PAGE: {
                if (resultCode == RESULT_OK && data.getExtras() != null && data.getExtras().getString("file_path") != null) {
                    pages.add(new PagesFragment.ImageItem(data.getExtras().getString("file_path")));
                    regions.add((Rect)data.getExtras().getParcelable("focus"));
                    dimensions = (Rect)data.getExtras().getParcelable("dimensions");
                    pagesFragment.update();
                }
            }
            break;
            case EDIT_PAGE: {

            }
            break;
        }
    }

    public void openMain() {
        if (!open) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, pagesFragment);
            transaction.commit();

            open = true;
        }

        pagesFragment.update();
    }

    public void goBack() {
        Bundle conData = new Bundle();
        ArrayList<String> pagePaths = new ArrayList<>();

        for (PagesFragment.ImageItem item : pages) pagePaths.add(item.getImageLocation());

        conData.putStringArrayList("pages", pagePaths);
        conData.putParcelableArrayList("regions", regions);
        conData.putParcelable("dimensions", dimensions);
        Intent intent = new Intent();
        intent.putExtras(conData);
        setResult(RESULT_OK, intent);
        finish();
    }

    public ArrayList<PagesFragment.ImageItem> getPages() {
        return pages;
    }

}
