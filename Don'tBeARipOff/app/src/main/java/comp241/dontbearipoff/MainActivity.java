package comp241.dontbearipoff;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity {

    private static final int LOADING_REQUEST = 1;
    private static final int LOADING_BOOKS_REQUEST = 2;

    private MainFragment mainFragment = new MainFragment();
    private boolean open, hasReturnedFromLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        Storage.setContext(this);

        if (!Storage.exists(null, "tessdata"))
            startLoadingActivity(getResources().getString(R.string.welcome_title), getResources().getString(R.string.welcome_subtitle), LOADING_REQUEST);
        else if (Storage.hasFiles("books"))
            startLoadingActivity(getResources().getString(R.string.loading_books_title), getResources().getString(R.string.loading_books_subtitle), LOADING_BOOKS_REQUEST);
        else openMain();
    }

    private void startLoadingActivity(String title, String subtitle, int result) {
        Intent intent = new Intent(this, LoadingActivity.class);

        intent.putExtra("title", title);
        intent.putExtra("subtitle", subtitle);
        intent.putExtra("taskType", result);

        startActivityForResult(intent, result);
    }

    @Override
    protected void onResume() {
        if (!hasReturnedFromLoading && Storage.hasFiles("books"))
            startLoadingActivity(getResources().getString(R.string.loading_books_title), getResources().getString(R.string.loading_books_subtitle), LOADING_BOOKS_REQUEST);
        else openMain();

        hasReturnedFromLoading = false;

        super.onResume();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case LOADING_REQUEST: {
                if (Storage.hasFiles("books"))
                    startLoadingActivity(getResources().getString(R.string.loading_books_title), getResources().getString(R.string.loading_books_subtitle), LOADING_BOOKS_REQUEST);
                else openMain();
            }
            break;
            case LOADING_BOOKS_REQUEST: {
                hasReturnedFromLoading = true;

                openMain();
            }
            break;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        mainFragment.update();
        super.onBackPressed();

        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        setTitle(R.string.app_name);
    }

    public void openMain() {
        if (!open) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.container, mainFragment);
            transaction.commit();
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
            setTitle(R.string.app_name);

            open = true;
        }

        mainFragment.update();
    }

    public void openDetails(MainFragment.BookItem book) {
        DetailsFragment detailsFragment = new DetailsFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        detailsFragment.setBook(book);
        transaction.addToBackStack(null);
        transaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
        transaction.replace(R.id.container, detailsFragment);
        transaction.commit();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Book Details");

        open = true;
    }

}
