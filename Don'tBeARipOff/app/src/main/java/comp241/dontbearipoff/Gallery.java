package comp241.dontbearipoff;

import java.util.ArrayList;

public abstract class Gallery {

    private static final String TAG = "BookApp/Gallery";

    public static ArrayList<MainFragment.BookItem> books = new ArrayList<>();

    //sorts alphabetically but s will be used for each category eg completelist.title to know what is being sorted, might not be needed
    public static ArrayList<MainFragment.BookItem> sortBy(ArrayList<MainFragment.BookItem> books, String s) {
        ArrayList<MainFragment.BookItem> sortedBooks = new ArrayList<>();

        if (books.size() > 0) {
            // check that at least one entry in list eg prevent crash due to trying to extract head
            sortedBooks.add(books.get(0));    // add the head of the original list to the sorted list to compare against
            int booksize = 0;        // used to fix double add of an entry if it is last on sorting order, could just break from else if cond / use bool checks

            for (int argCount = 1; argCount < books.size(); argCount++) {
                booksize++;    // increment as the sortedbook list grows

                for (int sortedCount = 0; sortedCount < booksize; sortedCount++) {
                    int ordCheck;

                    switch (s) {
                        case "author": {
                            ordCheck = books.get(argCount).getMeta().getAuthor().compareTo(sortedBooks.get(sortedCount).getMeta().getAuthor());
                        }
                        break;
                        default: {
                            ordCheck = books.get(argCount).getMeta().getTitle().compareTo(sortedBooks.get(sortedCount).getMeta().getTitle());
                        }
                        break;
                    }

                    if (ordCheck < 1) {
                        sortedBooks.add(sortedCount, books.get(argCount));

                        break;
                    } else if (sortedCount == (sortedBooks.size() - 1))
                        sortedBooks.add(books.get(argCount));
                }
            }

            return sortedBooks;
        } else return books;
    }

    public static ArrayList<MainFragment.BookItem> getByCategory(Category category) {
        if (category == null) return sortBy(books, "title");
        else {
            ArrayList<MainFragment.BookItem> toReturn = new ArrayList<>();

            for (MainFragment.BookItem book : books)
                if (category.hasCategory(book.getMeta())) toReturn.add(book);

            return sortBy(toReturn, "title");
        }
    }

}
