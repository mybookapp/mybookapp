package comp241.dontbearipoff;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.io.File;

import comp241.dontbearipoff.book.BookTask;
import comp241.dontbearipoff.book.InvalidBookException;

public class DetailsFragment extends Fragment implements BookTask.BookTaskListener, CategoryEditor.CategoryCallback {

    private static final String TAG = "DetailsFragment";

    private MainFragment.BookItem book;

    private View view;

    private TextView error;
    private EditText editAuthor, editDescription;
    private Menu menu;

    private AlertDialog dialog;
    private ProgressDialog progress;

    private CategoryEditor categoryEditor;

    public void setBook(MainFragment.BookItem book) {
        this.book = book;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        dialog = new AlertDialog.Builder(getActivity()).setTitle("Delete Book").setMessage("Are you sure you would like to delete the book?").setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                deleteBook();
                getActivity().onBackPressed();
            }
        }).setNegativeButton("Cancel", null).create();

        progress = new ProgressDialog(getActivity());
        progress.setTitle("Please Wait ...");
        progress.setMessage("Saving Details ...");
        progress.setCancelable(false);

        categoryEditor = new CategoryEditor(getActivity(), this);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.fragment_details, menu);

        this.menu = menu;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_remove: {
                if (!dialog.isShowing()) dialog.show();
            }
            return true;
            case R.id.action_edit: {
                view.findViewById(R.id.categoriesLabel).setVisibility(View.VISIBLE);
                view.findViewById(R.id.categories).setVisibility(View.VISIBLE);
                item.setVisible(false);
                menu.findItem(R.id.action_save).setVisible(true);
                categoryEditor.setVisible(true);
                setEnabled(true);
            }
            return true;
            case R.id.action_save: {
                view.findViewById(R.id.categoriesLabel).setVisibility(View.GONE);
                view.findViewById(R.id.categories).setVisibility(View.GONE);
                categoryEditor.setVisible(false);
                onSave();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void onSave() {
        if (editAuthor.getText().length() > 0) {
            menu.findItem(R.id.action_save).setVisible(false);
            menu.findItem(R.id.action_edit).setVisible(true);
            setEnabled(false);

            book.getBook().setExtractLocation(getActivity().getCacheDir() + File.separator + book.getMeta().getTitle());
            book.getMeta().setAuthor(editAuthor.getText().toString());
            book.getMeta().setDescription(editDescription.getText().toString());

            book.getBook().setBookTaskListener(this);
            progress.show();

            try {
                book.getBook().extractBook();
            } catch (InvalidBookException e) {
                e.printStackTrace();
                progress.dismiss();
            }
        } else {
            AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
            alert.setTitle("Error");
            alert.setMessage("Please fill out the book's author");
            alert.setPositiveButton("Ok", null);

            error.setText("* Please fill out the book's author *");
            error.setVisibility(View.VISIBLE);
            alert.create().show();
        }
    }

    private void setEnabled(boolean enabled) {
        editAuthor.setEnabled(enabled);
        editDescription.setEnabled(enabled);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_details, container, false);
        ViewGroup parent = (ViewGroup) view.findViewById(R.id.categories);

        categoryEditor.init(inflater, parent);

        EditText editTitle = (EditText) view.findViewById(R.id.bookName);
        editAuthor = (EditText) view.findViewById(R.id.bookAuthor);
        editDescription = (EditText) view.findViewById(R.id.bookDescription);
        error = (TextView) view.findViewById(R.id.bookError);

        book.getMeta().list();

        try {
            editTitle.setText(book.getMeta().getTitle());
        } catch (Exception e) {
            editTitle.setVisibility(View.GONE);
            view.findViewById(R.id.bookNameText).setVisibility(View.GONE);
        }

        try {
            editAuthor.setText(book.getMeta().getAuthor());
        } catch (Exception e) {
            editAuthor.setVisibility(View.GONE);
            view.findViewById(R.id.bookAuthorText).setVisibility(View.GONE);
        }

        try {
            editDescription.setText(book.getMeta().getDescription());
        } catch (Exception e) {
            editDescription.setVisibility(View.GONE);
            view.findViewById(R.id.bookDescriptionText).setVisibility(View.GONE);
        }

        return view;
    }

    private void deleteBook() {
        File bookFile = new File(book.getBookLocation());

        if (bookFile.delete()) Gallery.books.remove(book);
    }

    @Override
    public void onBookTaskProgress(BookTask.BookTaskType type, int progress) {
    }

    @Override
    public void onBookTaskComplete(BookTask.BookTaskType type, boolean success) {
        switch (type) {
            case EXTRACT_BOOK: {
                if (success) {
                    try {
                        Log.i("DetailsFragment", "Saving meta");
                        book.getMeta().save();
                    } catch (InvalidBookException e) {
                        e.printStackTrace();
                        progress.dismiss();
                    }
                } else {
                    Log.i("DetailsFragment", "Failed to extract book");
                    progress.dismiss();
                }
            }
            break;
            case SAVE_FILE: {
                if (success) {
                    try {
                        File bookFile = new File(book.getBookLocation());

                        Log.i("DetailsFragment", "Deleting book");

                        if (bookFile.delete()) {
                            Log.i("DetailsFragment", "Zipping book");
                            book.getBook().zipBook();
                        } else Log.i("DetailsFragment", "Failed to delete book");
                    } catch (InvalidBookException e) {
                        e.printStackTrace();
                        progress.dismiss();
                    }
                } else {
                    Log.i("DetailsFragment", "Failed to save meta");
                    progress.dismiss();
                }
            }
            break;
            case ZIP_BOOK: {
                if (success) Log.i("DetailsFragment", "Zipped book");
                else Log.i("DetailsFragment", "Failed to zip book");

                progress.dismiss();
            }
            break;
        }
    }

    @Override
    public void onAddCategory(Category category) {
        book.getMeta().set("metadata", category.getId(), "true");
    }

    @Override
    public void onRemoveCategory(Category category) {
        book.getMeta().set("metadata", category.getId(), "false");
    }

    @Override
    public boolean hasCategory(Category category) {
        return category.hasCategory(book.getMeta());
    }
}
