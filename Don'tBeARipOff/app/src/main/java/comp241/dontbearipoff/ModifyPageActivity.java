package comp241.dontbearipoff;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Toast;

import java.io.FileOutputStream;

public class ModifyPageActivity extends ActionBarActivity {

    private static final String TAG = "BookApp/ModifyPage";

    private int position;
    private String file_path;

    private Bitmap bitmap;
    private ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_modify_page);

        position = getIntent().getIntExtra("position", -1);
        file_path = getIntent().getStringExtra("file_path");
        bitmap = BitmapFactory.decodeFile(file_path);
        imageView = (ImageView) findViewById(R.id.modifyImageView);

        imageView.setImageBitmap(bitmap);

        ((SeekBar) findViewById(R.id.seekRotation)).setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                updateImage(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_modify_page, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_finalize_page:
                try {
                    FileOutputStream fos = new FileOutputStream(file_path);

                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                } catch (Exception error) {
                    Log.d(TAG, "File " + file_path + " not saved: " + error.getMessage());
                }

                Intent intent = new Intent();

                intent.putExtra("position", position);
                intent.putExtra("file_path", file_path);
                setResult(RESULT_OK, intent);
                finish();

                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateImage(int rotation) {
        Toast.makeText(this, "Rotated: " + (rotation - 90) + (char) 0x00B0, Toast.LENGTH_SHORT).show();
    }

}
