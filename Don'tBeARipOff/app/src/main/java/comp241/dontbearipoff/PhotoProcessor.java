package comp241.dontbearipoff;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;

import com.googlecode.tesseract.android.TessBaseAPI;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.RotatedRect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PhotoProcessor {
    static {
        if (!OpenCVLoader.initDebug()) {
            // TODO: handle initialization error
        }
    }

    private static final String TAG = "BookApp/PhotoProcessor";

    private Context mContext;

    private Bitmap mImage;
    private Rect mPage;
    private TessBaseAPI mTess;
    private ArrayList<String> mText;
    private ArrayList<Rect> mTextRegions;

    /**
     * The factor the image is downscaled by for image processing.
     */
    private static final int SCALE_FACTOR = 2;

    /**
     * The minimum accuracy required for a text region to be considered accurate.
     */
    private static final double ACCURACY_THRESHOLD = 0.2;

    public PhotoProcessor(Context ctx) {
        mContext = ctx;
        mTess = new TessBaseAPI();

        // TODO: attempt to automatically detect the language of the image instead of assuming English
        mTess.init(Storage.getFilesDir(null).getAbsolutePath(), "eng");

        mText = new ArrayList<>();
        mTextRegions = new ArrayList<>();
    }

    public Size getDimensions() {
        return new Size(mImage.getWidth(), mImage.getHeight());
    }

    public Rect getPage() {
        return mPage;
    }

    public Rect getPageRect() {
        return mPage;
    }

    public ArrayList<String> getText() {
        if (mText == null) return new ArrayList<>();
        return mText;
    }

    // TODO: depaper the image with openCV to increase OCR quality
    // TODO: layout analysis (port relevant parts of Ocropy or OCRFeeder)
    // TODO: extract images
    public void processFastText() {
        mTess.setImage(mImage);
        String x = mTess.getUTF8Text();
        mText.add(x);
    }

    public void processComplexText() {
        Mat image = new Mat();
        Utils.bitmapToMat(mImage, image);

        Log.v(TAG, "Extracting page region.");

        /*Mat m, rotated = new Mat();
        double angle = mPage.angle;
        Size rectSize = mPage.size;

        if (angle < -45.) {
            angle += 90.0;
            double temp = rectSize.width;
            rectSize.width = rectSize.height;
            rectSize.height = temp;
        }

        m = Imgproc.getRotationMatrix2D(mPage.center, angle, 1.0);
        Imgproc.warpAffine(image, rotated, m, image.size(), Imgproc.INTER_CUBIC);
        Imgproc.getRectSubPix(rotated, rectSize, mPage.center, image);*/

        if (mPage != null)
            image = new Mat(image, mPage);

        Log.v(TAG, "Attempting to find text regions.");

        ArrayList<Rect> boundRect = new ArrayList<>();
        Mat gray = new Mat();
        Mat sobel = new Mat();
        Mat threshold = new Mat();
        Mat element = new Mat();

        Imgproc.cvtColor(image, gray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.Sobel(gray, sobel, CvType.CV_8U, 1, 0, 3, 1, 0, Imgproc.BORDER_DEFAULT);
        Imgproc.threshold(sobel, threshold, 0, 255, Imgproc.THRESH_OTSU | Imgproc.THRESH_BINARY);
        element = Imgproc.getStructuringElement(Imgproc.MORPH_RECT, new Size(17, 3));

        Imgproc.morphologyEx(threshold, threshold, Imgproc.MORPH_CLOSE, element);

        List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
        Imgproc.findContours(threshold, contours, new Mat(), Imgproc.RETR_EXTERNAL/*0*/, Imgproc.CHAIN_APPROX_NONE/*1*/);

        Iterator<MatOfPoint> each = contours.iterator();
        Log.v(TAG, "Analyzing rectangles...");
        while (each.hasNext()) {
            MatOfPoint wrapper = each.next();

            if (wrapper.toArray().length > 100) {
                MatOfPoint2f mpf = new MatOfPoint2f(wrapper.toArray());
                MatOfPoint2f out = new MatOfPoint2f();
                Imgproc.approxPolyDP(mpf, out, 3, true);
                Rect appRect = Imgproc.boundingRect(new MatOfPoint(out.toArray()));
                if ((appRect.width > appRect.height) && appRect.height >= 12) {
                    boundRect.add(appRect);
                }

                // TODO: filter extremely lopsided regions (i.e. see: http://i.imgur.com/2qZJtze.jpg)
            }
        }

        Pattern pattern = Pattern.compile("\\w+");
        TrackedSpellCheck spellCheck = new TrackedSpellCheck(mContext);
        for (Rect r : boundRect) {
            Bitmap subregion = Bitmap.createBitmap(mImage, r.x, r.y, r.width, r.height);
            mTess.setImage(subregion);

            String ocr = mTess.getUTF8Text();
            Matcher matcher = pattern.matcher(ocr);
            StringBuilder sb = new StringBuilder();

            while (matcher.find()) {
                String match = matcher.group().toLowerCase();
                spellCheck.check(match);
                sb.append(match);
            }

            mTextRegions.add(r);
            mText.add(ocr);
            //if (matcher.matches()) {
            //    if (spellCheck.getAccuracy() >= ACCURACY_THRESHOLD || ((double) sb.toString().length() / ocr.length()) >= ACCURACY_THRESHOLD) {
            //        mTextRegions.add(r);
            //        mText.add(matcher.group());
            //    }
            //}

            spellCheck.reset();
            //Core.rectangle(image, new Point(r.x, r.y), new Point(r.x + r.width, r.y + r.height), new Scalar(255, 0, 255), 2);
        }

        mTess.setImage(mImage);

        //Highgui.imwrite(Storage.getFilesDir(Environment.DIRECTORY_PICTURES) + File.separator + "TEST2.jpg", image);
        //Log.d(TAG, Storage.getFilesDir(Environment.DIRECTORY_PICTURES) + "TEST2.jpg");
    }

    public void processImages() {
        Mat image = new Mat();
        Utils.bitmapToMat(mImage, image);

        // white out text regions
        for (Rect r : mTextRegions) {
            Core.rectangle(image, new Point(r.x, r.y), new Point(r.x + r.width, r.y + r.height),
                    new Scalar(255, 255, 255), Core.FILLED);
        }

        Imgproc.threshold(image, image, 153, 255, Imgproc.THRESH_BINARY);
    }

    public void setImage(Bitmap bmp) {
        mImage = bmp;
        mTess.setImage(mImage);
    }

    public void setPageRegion(android.graphics.Rect region) {
        mPage = new Rect();
        mPage.x = region.left;
        mPage.y = region.top;
        mPage.height = region.height();
        mPage.width = region.width();
    }
}
