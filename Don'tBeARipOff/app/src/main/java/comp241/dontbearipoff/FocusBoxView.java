package comp241.dontbearipoff;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;
import android.view.View;

import org.opencv.core.Point;

public class FocusBoxView extends View {
    private static final String TAG = "BookApp/FocusBoxView";

    // each corner measures twice this size for width/height
    public static final int CORNER_SIZE = 15;

    private static final int BUFFER = CORNER_SIZE + 35;

    // minimum size of the box
    private static final int MIN_FOCUS_BOX_HEIGHT = 2 * BUFFER + CORNER_SIZE;
    private static final int MIN_FOCUS_BOX_WIDTH = MIN_FOCUS_BOX_HEIGHT * 2;

    public static final int NONE = -1;
    public static final int TOP_LEFT = 0;
    public static final int TOP_RIGHT = 1;
    public static final int BOTTOM_RIGHT = 2;
    public static final int BOTTOM_LEFT = 3;

    /**
     * Sometimes the user's dragging can become desynced with the focus box due to the delay that drawing causes.
     * Therefore, once a corner starts moving, it is deemed to be 'locked' and any subsequent movement will always move
     * that corner, regardless of where the user's finger is.
     */
    private int mCorner = NONE;

    private Rect mRect;
    private Rect mConstraint;

    private SurfaceView mSurfaceView;

    private View.OnTouchListener mTouchListener;

    public FocusBoxView(Context context) {
        this(context, null);
    }

    public FocusBoxView(Context context, SurfaceView sv) {
        super(context);

        mSurfaceView = sv;

        mTouchListener = new OnTouchListener() {
            int lastX = -1, lastY = -1;

            @Override
            public boolean onTouch(View v, MotionEvent e) {
                switch (e.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        lastX = (int)e.getX();
                        lastY = (int)e.getY();
                        return true;

                    case MotionEvent.ACTION_UP:
                        lastX = -1;
                        lastY = -1;
                        mCorner = NONE;
                        break;

                    case MotionEvent.ACTION_MOVE:
                        int curX = (int)e.getX();
                        int curY = (int)e.getY();

                        //Log.d(TAG, "Touch event: (" + curX + ", " + curY + ")");

                        Rect bounding = getBoxRect();
                        Rect rect = new Rect(bounding.left, bounding.top, bounding.right, bounding.bottom);
                        Rect rectW = new Rect(bounding.left, bounding.top, bounding.right, bounding.bottom);
                        Rect rectH = new Rect(bounding.left, bounding.top, bounding.right, bounding.bottom);

                        if (lastX >= 0) {
                            Point current = new Point(curX, curY);
                            Point last = new Point(lastX, lastY);

                            int dy = curY - lastY;
                            int dx = curX - lastX;

                            if (mCorner == NONE) {
                                if (inside(current, last, new Point(rect.left, rect.top)))
                                    mCorner = TOP_LEFT;
                                else if (inside(current, last, new Point(rect.right, rect.top)))
                                    mCorner = TOP_RIGHT;
                                else if (inside(current, last, new Point(rect.right, rect.bottom)))
                                    mCorner = BOTTOM_RIGHT;
                                else if (inside(current, last, new Point(rect.left, rect.bottom)))
                                    mCorner = BOTTOM_LEFT;
                            }

                            switch (mCorner) {
                                case TOP_LEFT:
                                    rect.left += dx;
                                    rect.top += dy;

                                    rectW.left += dx;
                                    rectH.top += dy;
                                    break;

                                case TOP_RIGHT:
                                    rect.right += dx;
                                    rect.top += dy;

                                    rectW.right += dx;
                                    rectH.top += dy;
                                    break;

                                case BOTTOM_RIGHT:
                                    rect.right += dx;
                                    rect.bottom += dy;

                                    rectW.right += dx;
                                    rectH.bottom += dy;
                                    break;

                                case BOTTOM_LEFT:
                                    rect.left += dx;
                                    rect.bottom += dy;

                                    rectW.left += dx;
                                    rectH.bottom += dy;
                                    break;
                            }

                            if (isRectSane(rect)) {
                                mRect = rect;
                            } else if (isRectSane(rectW)) {
                                mRect = rectW;
                            } else if (isRectSane(rectH)) {
                                mRect = rectH;
                            }
                            boundsCheck();

                            lastX = curX;
                            lastY = curY;

                            mSurfaceView.invalidate();

                            Log.d(TAG, "Top left now: (" + mRect.left + ", " + mRect.top + ")");
                        }

                        return true;
                }

                return false;
            }
        };
    }

    private void boundsCheck() {
        // ensure that corners do not go off the edge of the surface view
        if (hasConstraints()) {
            mRect.right = Math.min(mRect.right, mConstraint.right);
            mRect.bottom = Math.min(mRect.bottom, mConstraint.bottom);
        }

        mRect.left = Math.abs(mRect.left);
        mRect.top = Math.abs(mRect.top);
    }

    private Rect getBoxRect() {
        if (mRect == null) {
            // initialize the view in the center of the preview
            int height = MIN_FOCUS_BOX_HEIGHT;
            int width = MIN_FOCUS_BOX_WIDTH;

            int left = (mSurfaceView.getWidth() - width) / 2;
            int top = (mSurfaceView.getHeight() - height) / 2;

            mRect = new Rect(left, top, left + width, top + height);
        }

        return mRect;
    }

    public int getLockedCorner() {
        return mCorner;
    }

    public Rect getSelection() {
        return getBoxRect();
    }

    public OnTouchListener getTouchListener() {
        return mTouchListener;
    }

    public boolean hasConstraints() {
        return mConstraint != null;
    }

    private boolean inside(Point p1, Point p2, Point area) {
        return (inside(p1, area) || inside(p2, area));
    }

    private boolean inside(Point point, Point area) {
        return (point.x >= (area.x - BUFFER) && point.x <= (area.x + BUFFER) &&
                point.y >= (area.y - BUFFER) && point.y <= (area.y + BUFFER));
    }

    private boolean isRectSane(Rect rect) {
        return ((rect.right - rect.left) >= MIN_FOCUS_BOX_WIDTH &&
                (rect.bottom - rect.top) >= MIN_FOCUS_BOX_HEIGHT);
    }

    public void setConstraints(Rect constraint) {
        mConstraint = constraint;
        Log.d(TAG, "Constraint: (" + constraint.right + ", " + constraint.bottom + ")");
    }
}