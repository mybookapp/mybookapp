package comp241.dontbearipoff;

import comp241.dontbearipoff.book.Book;

public enum Category {

    BOOK("book", "Books"),
    COMIC("comic", "Comics"),
    RECIPE("recipe", "Recipes"),
    OTHER("other", "Others");

    private String id, name;

    Category(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean hasCategory(Book.BookMeta meta) {
        try {
            String tag = meta.get(id);

            return tag.equals("true");
        } catch (Exception e) {
            return false;
        }
    }

    public static Category getById(String id) {
        for (Category category : values()) if (category.id.equals(id)) return category;

        return null;
    }

}
