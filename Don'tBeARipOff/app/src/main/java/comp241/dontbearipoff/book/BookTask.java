package comp241.dontbearipoff.book;

import android.os.AsyncTask;

public abstract class BookTask extends AsyncTask<Void, Integer, Void> {

    protected Book book;
    protected boolean success;

    private BookTaskType taskType;
    private BookTaskListener taskListener;

    public BookTask(Book book, BookTaskType taskType, BookTaskListener taskListener) {
        this.book = book;
        this.taskType = taskType;
        this.taskListener = taskListener;
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        if (taskListener != null)
            taskListener.onBookTaskProgress(taskType, values[values.length - 1]);
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        if (taskListener != null) taskListener.onBookTaskComplete(taskType, success);
    }

    public enum BookTaskType {

        EXTRACT_BOOK, SAVE_BOOK, SAVE_FILE, ZIP_BOOK

    }

    public interface BookTaskListener {

        void onBookTaskProgress(BookTaskType type, int progress);

        void onBookTaskComplete(BookTaskType type, boolean success);

    }

}
