package comp241.dontbearipoff.book;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

public class Book {

    private String location, extractLocation, contentPath;

    private BookTask.BookTaskListener taskListener;

    private boolean validBook = true;
    private boolean extracted;

    private boolean newBook;
    private String epubLocation;
    private BookMeta meta;


    public Book(Bitmap cover, String location, String epubLocation) throws ParserConfigurationException {
        this(cover, location, epubLocation, null);
    }

    public Book(Bitmap cover, String location, String epubLocation, BookTask.BookTaskListener taskListener) throws ParserConfigurationException {
        this.location = location;
        this.taskListener = taskListener;
        this.newBook = true;
        this.epubLocation = epubLocation;
        this.meta = new BookMeta(this);

        meta.setCover(cover, location);
    }

    public Book(String epubLocation) {
        this(epubLocation, null, false, null);
    }

    public Book(String epubLocation, String extractLocation) {
        this(epubLocation, extractLocation, true, null);
    }

    public Book(String epubLocation, String extractLocation, BookTask.BookTaskListener taskListener) {
        this(epubLocation, extractLocation, true, taskListener);
    }

    public Book(String epubLocation, String extractLocation, boolean extract) {
        this(epubLocation, extractLocation, extract, null);
    }

    public Book(String epubLocation, String extractLocation, boolean extract, BookTask.BookTaskListener taskListener) {
        this.epubLocation = epubLocation;
        this.extractLocation = extractLocation;
        this.taskListener = taskListener;

        if (extract) {
            try {
                extractBook();
            } catch (InvalidBookException e) {
                e.printStackTrace();
            }
        } else parseContentPath();
    }

    public void extractBook() throws InvalidBookException {
        if (!validBook) throw new InvalidBookException();
        else if (extractLocation == null || extractLocation.isEmpty())
            throw new InvalidBookException("No extract location set for the book");

        new BookTask(this, BookTask.BookTaskType.EXTRACT_BOOK, taskListener) {

            @Override
            protected Void doInBackground(Void... params) {
                byte[] buffer = new byte[4096];

                int length;

                float entries;
                float current = 0;

                FileInputStream fin;
                ZipInputStream zin;

                try {
                    File extractFile = new File(book.epubLocation);
                    File extractLocation = new File(book.extractLocation);

                    entries = new ZipFile(extractFile).size();

                    if (extractLocation.exists()) extractLocation.delete();

                    fin = new FileInputStream(extractFile);
                    zin = new ZipInputStream(fin);

                    ZipEntry ze;

                    while ((ze = zin.getNextEntry()) != null) {
                        current++;

                        if (ze.isDirectory()) {
                            File file = new File(extractLocation, ze.getName());

                            file.mkdirs();
                        } else {
                            File toWrite = new File(extractLocation, ze.getName());
                            toWrite.getParentFile().mkdirs();

                            FileOutputStream fout = new FileOutputStream(toWrite);

                            while ((length = zin.read(buffer)) > 0) fout.write(buffer, 0, length);

                            zin.closeEntry();
                            fout.close();
                        }

                        publishProgress((int) (current / entries * 100));
                    }

                    zin.close();

                    book.extracted = true;
                    success = true;
                } catch (Exception e) {
                    e.printStackTrace();

                    book.validBook = false;
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                book.parseContentPath();

                super.onPostExecute(aVoid);
            }

        }.execute();
    }

    private void saveXML(final Document document, final String relativeLocation) throws InvalidBookException {
        if (!validBook) throw new InvalidBookException();

        new BookTask(this, BookTask.BookTaskType.SAVE_FILE, taskListener) {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    Transformer transformer = TransformerFactory.newInstance().newTransformer();

                    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

                    DOMSource source = new DOMSource(document);
                    File toWrite = new File(extractLocation + File.separator + relativeLocation);

                    Log.i("BookClass", "Saving XML: '" + toWrite.getPath() + "'");

                    StreamResult result = new StreamResult(toWrite.getPath());

                    transformer.transform(source, result);

                    success = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }
        }.execute();
    }

    private InputStream getFileFromZip(String filename) throws IOException {
        ZipFile zipFile = new ZipFile(epubLocation);
        ZipEntry zipEntry = zipFile.getEntry(filename);

        if (zipEntry != null) return zipFile.getInputStream(zipEntry);

        return null;
    }

    public InputStream getFile(String filename) throws IOException {
        if (extracted) return new FileInputStream(extractLocation + File.separator + filename);
        else return getFileFromZip(filename);
    }

    private Document parseXml(String xmlLocation) {
        if (!validBook) return null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            InputStream inputStream = getFile(xmlLocation);

            if (inputStream == null) {
                validBook = false;

                return null;
            }

            return builder.parse(inputStream);

        } catch (Exception e) {
            e.printStackTrace();

            validBook = false;
        }

        return null;
    }

    private void parseContentPath() {
        Document document = parseXml("META-INF/container.xml");

        if (document != null) {
            contentPath = document.getElementsByTagName("rootfile").item(0).getAttributes().item(0).getTextContent();

            if (contentPath == null || contentPath.isEmpty()) validBook = false;
        }
    }

    public void setBookTaskListener(BookTask.BookTaskListener taskListener) {
        this.taskListener = taskListener;
    }

    public void setExtractLocation(String extractLocation) {
        this.extractLocation = extractLocation;
    }

    public String getExtractLocation() {
        return extractLocation;
    }

    public boolean isValid() {
        return validBook;
    }

    public BookMeta getMeta() throws InvalidBookException {
        if (!validBook) throw new InvalidBookException();

        if (meta == null) {
            Document contentDocument = parseXml(contentPath);

            if (contentDocument == null) {
                validBook = false;

                throw new InvalidBookException("Content path is invalid");
            }

            meta = new BookMeta(this, contentDocument);
        }

        return meta;
    }

    public void zipBook() throws InvalidBookException {
        if (!validBook) throw new InvalidBookException();

        new BookTask(this, BookTask.BookTaskType.ZIP_BOOK, taskListener) {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    File extractFile = new File(extractLocation);
                    ZipOutputStream zipOut = new ZipOutputStream(new FileOutputStream(epubLocation));

                    Log.i("BookClass", "Zipping: '" + extractLocation + "' to '" + epubLocation + "'");

                    for (File file : extractFile.listFiles()) {
                        if (file.isDirectory()) zipDir(zipOut, "", file);
                        else zipFile(zipOut, "", file);
                    }

                    zipOut.close();

                    success = true;
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }
        }.execute();
    }

    private void zipDir(ZipOutputStream zos, String path, File dir) throws IOException {
        if (!dir.canRead()) return;

        File[] files = dir.listFiles();
        path = buildPath(path, dir.getName());

        for (File source : files) {
            if (source.isDirectory()) zipDir(zos, path, source);
            else zipFile(zos, path, source);
        }
    }

    private void zipFile(ZipOutputStream zos, String path, File file) throws IOException {
        if (!file.canRead()) return;

        System.out.println("Compressing " + file.getName());
        zos.putNextEntry(new ZipEntry(buildPath(path, file.getName())));

        FileInputStream fis = new FileInputStream(file);

        byte[] buffer = new byte[4092];
        int byteCount = 0;
        while ((byteCount = fis.read(buffer)) != -1) {
            zos.write(buffer, 0, byteCount);
        }

        fis.close();
        zos.closeEntry();
    }

    private String buildPath(String path, String file) {
        return path == null || path.isEmpty() ? file : (path + File.separator + file);
    }

    public void saveBook() throws InvalidBookException {
        if (!validBook) throw new InvalidBookException();

        new BookTask(this, BookTask.BookTaskType.SAVE_BOOK, taskListener) {
            @Override
            protected Void doInBackground(Void... params) {
                try {
                    File buildingDirectory = new File(location);
                    byte[] buffer = new byte[1024];

                    buildingDirectory.delete();
                    buildingDirectory.mkdirs();

                    saveMimetype(location);
                    saveTOC(location);
                    saveContainer(location);
                    saveContent(getMeta().getMetaForSaving(), location);

                    ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(epubLocation));

                    zipCoverImage(zipOutputStream);
                    zipPages(zipOutputStream);
                    zipImages(zipOutputStream);

                    for (int i = 0; i < buildingDirectory.listFiles().length; i++) {
                        Log.i("File", buildingDirectory.listFiles()[i].getName());

                        if (buildingDirectory.listFiles()[i].isDirectory()) {
                            zipSubDirectory(buildingDirectory.listFiles()[i], zipOutputStream);

                            continue;
                        } else if (buildingDirectory.listFiles()[i].getName().equals("Cover.jpeg"))
                            continue;

                        FileInputStream in = new FileInputStream(buildingDirectory.listFiles()[i].getPath());
                        zipOutputStream.putNextEntry(new ZipEntry(buildingDirectory.listFiles()[i].getName()));

                        int length;

                        while ((length = in.read(buffer)) > 0)
                            zipOutputStream.write(buffer, 0, length);

                        zipOutputStream.closeEntry();
                        in.close();
                    }

                    zipOutputStream.close();
                } catch (Exception e) {
                    e.printStackTrace();

                    success = false;
                }

                return null;
            }
        }.execute();
    }

    private void zipCoverImage(ZipOutputStream zip) throws IOException {
        byte[] buffer = new byte[1024];
        File cover = new File(meta.coverPath);
        FileInputStream in = new FileInputStream(cover);

        zip.putNextEntry(new ZipEntry(cover.getName()));

        int length;

        while ((length = in.read(buffer)) > 0) zip.write(buffer, 0, length);

        zip.closeEntry();
        in.close();
    }

    private void zipPages(ZipOutputStream zip) throws IOException {
        byte[] buffer = new byte[1024];

        for (BookMeta.ContentsItem c : meta.getTableOfContents()) {
            File f = new File(c.path);

            int i = f.getPath().lastIndexOf('.');
            String extensionName = f.getPath().substring(i);

            FileInputStream in = new FileInputStream(f);
            zip.putNextEntry(new ZipEntry("pages/" + c.name + extensionName));

            int length;

            while ((length = in.read(buffer)) > 0) zip.write(buffer, 0, length);

            zip.closeEntry();
            in.close();
        }
    }

    private void zipImages(ZipOutputStream zip) throws IOException {
        byte[] buffer = new byte[1024];

        for (BookMeta.ContentsItem c : meta.images) {
            File f = new File(c.path);

            FileInputStream in = new FileInputStream(f);
            zip.putNextEntry(new ZipEntry("images/" + c.name + c.getExtension()));

            int length;

            while ((length = in.read(buffer)) > 0) zip.write(buffer, 0, length);

            zip.closeEntry();
            in.close();
        }
    }

    private void zipSubDirectory(File toZip, ZipOutputStream zip) throws IOException {
        byte[] buffer = new byte[1024];

        for (int i = 0; i < toZip.listFiles().length; i++) {
            try {
                Log.i("File", toZip.listFiles()[i].getName());

                if (toZip.listFiles()[i].isDirectory()) {
                    zipSubDirectory(toZip.listFiles()[i], zip);

                    continue;
                }

                FileInputStream in = new FileInputStream(toZip.listFiles()[i].getPath());
                zip.putNextEntry(new ZipEntry(toZip.getName() + "/" + toZip.listFiles()[i].getName()));

                int length;

                while ((length = in.read(buffer)) > 0) zip.write(buffer, 0, length);

                zip.closeEntry();
                in.close();
            } catch (ZipException e) {
            }
        }
    }

    public void addPage(InputStream pageInput, String pageName) {
        String pagePath = location + "/" + pageName + ".html";

        try {
            FileOutputStream out = new FileOutputStream(location + "/" + pageName + ".html");

            int content;

            while ((content = pageInput.read()) != -1) out.write(content);

            meta.tableOfContents.add(new BookMeta.ContentsItem(pagePath, pageName));
        } catch (IOException e) {
            Log.i("Page adding", " = " + e.getMessage());
        }
    }

    public void addImagePage(String path, String pageName) {
        String pagePath = location + "/" + pageName + ".html";

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(pagePath));

            writer.write("<?xml version='1.0' encoding='utf-8'?>\n" +
                    "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
                    "  <head>\n" +
                    "    <title>Cover 1</title>\n" +
                    "    <meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>\n" +
                    "    <style>\n" +
                    "      .page_image {\n" +
                    "        max-width: 100%;\n" +
                    "        max-height: 100%;\n" +
                    "        bottom: 0;\n" +
                    "        left: 0;\n" +
                    "        margin: auto;\n" +
                    "        overflow: auto;\n" +
                    "        position: fixed;\n" +
                    "        right: 0;\n" +
                    "        top: 0;\n" +
                    "      }\n" +
                    "    </style>\n" +
                    "  </head>\n" +
                    "  <body>\n" +
                    "    <p><img src=\"../images/" + pageName + ".jpg\" alt=\"images\" class=\"page_image\"/></p>\n" +
                    "  </body>\n" +
                    "</html>\n");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        meta.tableOfContents.add(new BookMeta.ContentsItem(pagePath, pageName));
        meta.images.add(new BookMeta.ContentsItem(path, pageName));
    }

    private void saveTOC(String location) throws IOException, ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document TOC = builder.newDocument();

        //root element
        Element root = TOC.createElement("ncx");
        root.setAttribute("version", "2005-1");
        root.setAttribute("xmlns", "http://www.daisy.org/z3986/2005/ncx/");
        root.setAttribute("xml:lang", "eng");
        TOC.appendChild(root);

        //child rootfiles
        Element navMap = TOC.createElement("navMap");
        root.appendChild(navMap);

        for (BookMeta.ContentsItem c : meta.tableOfContents) {
            Element navPoint = TOC.createElement("navPoint");
            navPoint.setAttribute("class", c.cClass);
            navPoint.setAttribute("playOrder", String.valueOf(meta.tableOfContents.indexOf(c)));
            navPoint.setAttribute("id", "navPoint" + meta.tableOfContents.indexOf(c));
            navMap.appendChild(navPoint);

            Element navLabel = TOC.createElement("navLabel");
            navPoint.appendChild(navLabel);

            Element navText = TOC.createElement("text");
            navText.setTextContent(c.name);
            navLabel.appendChild(navText);

            Element navContent = TOC.createElement("content");
            navContent.setAttribute("src", c.path);
            navPoint.appendChild(navContent);
        }


        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        DOMSource source = new DOMSource(TOC);
        File toWrite = new File(location + "/toc.ncx");
        StreamResult result = new StreamResult(toWrite.getPath());

        transformer.transform(source, result);
    }

    private void saveMimetype(String location) throws IOException {
        PrintWriter out = new PrintWriter(location + "/mimetype");

        out.println("application/epub+zip");
        out.close();
    }

    private void saveContainer(String location) throws IOException, ParserConfigurationException, TransformerException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document container = builder.newDocument();

        //root element
        Element root = container.createElement("container");
        root.setAttribute("version", "1.0");
        root.setAttribute("xmlns", "urn:oasis:names:tc:opendocument:xmlns:container");
        container.appendChild(root);

        //child rootfiles
        Element rootfiles = container.createElement("rootfiles");
        root.appendChild(rootfiles);

        //child rootfile
        Element rootfile = container.createElement("rootfile");
        rootfile.setAttribute("full-path", "content.opf");
        rootfile.setAttribute("media-type", "application/oebps-package+xml");
        rootfiles.appendChild(rootfile);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        DOMSource source = new DOMSource(container);
        File toWrite = new File(location + "/META-INF/container.xml");
        toWrite.getParentFile().mkdir();
        StreamResult result = new StreamResult(toWrite.getPath());

        transformer.transform(source, result);
    }

    public Document createContent() throws ParserConfigurationException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document container = builder.newDocument();

        //root element
        Element root = container.createElement("package");
        root.setAttribute("version", "2.0");
        root.setAttribute("xmlns", "http://www.idpf.org/2007/opf");
        root.setAttribute("unique-identifier", "uuid_id");
        container.appendChild(root);

        //child metadata
        Element metaData = container.createElement("metadata");
        metaData.setAttribute("xmlns:opf", "http://www.idpf.org/2007/opf");
        metaData.setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
        root.appendChild(metaData);

        //adding metadata
        Element eAuthor = container.createElement("dc:creator");
        eAuthor.setTextContent(meta.author);
        metaData.appendChild(eAuthor);

        Element eTitle = container.createElement("dc:title");
        eTitle.setTextContent(meta.title);
        metaData.appendChild(eTitle);

        Element eDescription = container.createElement("dc:description");
        eAuthor.setTextContent(meta.description);
        metaData.appendChild(eDescription);

        Element eCover = container.createElement("meta");
        eCover.setAttribute("name", "cover");
        eCover.setAttribute("content", "cover");
        metaData.appendChild(eCover);

        Element spine = container.createElement("spine");
        spine.setAttribute("tox", "ncx");
        root.appendChild(spine);

        Element manifest = container.createElement("manifest");
        root.appendChild(manifest);

        Element mCover = container.createElement("item");
        mCover.setAttribute("href", new File(meta.coverPath).getName());
        mCover.setAttribute("id", "cover");
        mCover.setAttribute("media-type", "image/jpeg");
        manifest.appendChild(mCover);

        for (BookMeta.ContentsItem c : meta.tableOfContents) {
            Element spineItem = container.createElement("itemref");
            spineItem.setAttribute("idref", c.name);
            spine.appendChild(spineItem);
            Element mItem = container.createElement("item");
            mItem.setAttribute("href", "pages/" + c.name + c.getExtension());
            mItem.setAttribute("id", c.name);
            mItem.setAttribute("media-type", "application/xhtml+xml");
            manifest.appendChild(mItem);
        }

        return container;
    }

    private void saveContent(Document document, String location) throws TransformerException {
        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

        DOMSource source = new DOMSource(document);
        File toWrite = new File(location + "/content.opf");
        StreamResult result = new StreamResult(toWrite.getPath());

        transformer.transform(source, result);
    }

    public int pageCount() {
        return meta.spineList.size();
    }

    public static class BookMeta {

        private Book book;
        private Document metaDocument;

        private String title, author, description, coverPath;

        private Element root, metaData;

        private List<String> spineList;
        private List<ContentsItem> tableOfContents;
        private List<ContentsItem> images;

        public BookMeta(Book book) throws ParserConfigurationException {
            this.book = book;
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            this.metaDocument = builder.newDocument();

            root = metaDocument.createElement("package");
            root.setAttribute("version", "2.0");
            root.setAttribute("xmlns", "http://www.idpf.org/2007/opf");
            root.setAttribute("unique-identifier", "uuid_id");
            metaDocument.appendChild(root);

            metaData = metaDocument.createElement("metadata");
            metaData.setAttribute("xmlns:opf", "http://www.idpf.org/2007/opf");
            metaData.setAttribute("xmlns:dc", "http://purl.org/dc/elements/1.1/");
            root.appendChild(metaData);

            this.title = "";
            this.author = "";
            this.description = "";
            this.spineList = new ArrayList<>();
            this.tableOfContents = new ArrayList<>();
            this.images = new ArrayList<>();
        }

        public BookMeta(Book book, Document metaDocument) {
            this.book = book;
            this.metaDocument = metaDocument;
        }

        public void set(String parent, String tagName, String content) {
            if (metaDocument.getElementsByTagName(tagName).item(0) != null)
                metaDocument.getElementsByTagName(tagName).item(0).setTextContent(content);
            else {
                Element element = metaDocument.createElement(tagName);

                element.setTextContent(content);
                metaDocument.getElementsByTagName(parent).item(0).appendChild(element);
            }
        }

        public String get(String tagName) {
            return metaDocument.getElementsByTagName(tagName).item(0).getTextContent();
        }

        public String getTitle() {
            return title == null ? title = metaDocument.getElementsByTagName("dc:title").item(0).getTextContent() : title;
        }

        public void setTitle(String title) {
            this.title = title;

            set("metadata", "dc:title", title);
        }

        public String getAuthor() {
            return author == null ? author = metaDocument.getElementsByTagName("dc:creator").item(0).getTextContent() : author;
        }

        public void setAuthor(String author) {
            this.author = author;

            set("metadata", "dc:creator", author);
        }

        public String getDescription() {
            return description == null ? description = metaDocument.getElementsByTagName("dc:description").item(0).getTextContent() : description;
        }

        public void setDescription(String description) {
            this.description = description;

            set("metadata", "dc:description", description);
        }

        public List<String> getSpineList() {
            if (spineList == null) {
                spineList = new ArrayList<>();

                NodeList spine;

                if (metaDocument.getElementsByTagName("spine").getLength() == 0)
                    spine = metaDocument.getElementsByTagName("opf:spine").item(0).getChildNodes();
                else spine = metaDocument.getElementsByTagName("spine").item(0).getChildNodes();

                for (int i = 0; i < spine.getLength(); i++) {
                    if (spine.item(i).getNodeType() == Node.ELEMENT_NODE) {
                        String itemName = spine.item(i).getAttributes().item(0).getTextContent();
                        Node item = metaDocument.getElementById(itemName);

                        for (int j = 0; j < item.getAttributes().getLength(); j++) {
                            if (item.getAttributes().item(j).getNodeName().equals("href")) {
                                spineList.add(book.contentPath.replace("content.opf", item.getAttributes().item(j).getTextContent()));

                                break;
                            }
                        }
                    }
                }
            }

            return spineList;
        }

        public List<ContentsItem> getTableOfContents() {
            if (tableOfContents == null) {
                String TOCPath = book.contentPath.replace("content.opf", "toc.ncx");
                Document document = book.parseXml(TOCPath);

                if (document == null) return null;

                tableOfContents = new ArrayList<>();
                NodeList navPoints = document.getElementsByTagName("navPoint");

                for (int i = 0; i < navPoints.getLength(); i++) {
                    Node node = navPoints.item(i);
                    String itemName = node.getChildNodes().item(1).getChildNodes().item(1).getTextContent();
                    String itemPath = node.getChildNodes().item(3).getAttributes().item(0).getTextContent();

                    tableOfContents.add(new ContentsItem(itemPath, itemName));
                }
            }

            return tableOfContents;
        }

        public String getCoverPath() {
            if (coverPath == null) {
                NodeList metadata;

                if (metaDocument.getElementsByTagName("metadata").getLength() == 0)
                    metadata = metaDocument.getElementsByTagName("opf:metadata").item(0).getChildNodes();
                else
                    metadata = metaDocument.getElementsByTagName("metadata").item(0).getChildNodes();

                for (int i = 0; i < metadata.getLength(); i++) {
                    String nodeName = metadata.item(i).getNodeName();

                    if (nodeName.equals("opf:meta") || nodeName.equals("meta")) {
                        Node metaItem = metadata.item(i);

                        switch (metaItem.getAttributes().item(0).getTextContent()) {
                            case "cover":
                                String coverID = metaItem.getAttributes().item(1).getTextContent();
                                coverPath = book.contentPath.replace("content.opf", metaDocument.getElementById(coverID).getAttribute("href"));
                                break;
                        }
                    }
                }
            }

            return coverPath;
        }

        public Bitmap getCover() {
            if (getCoverPath() == null) return null;

            try {
                return BitmapFactory.decodeStream(book.getFile(getCoverPath()));
            } catch (Exception e) {
                e.printStackTrace();
            }

            return null;
        }

        public boolean setCover(Bitmap cover, String location) {
            coverPath = location + "/Cover.jpeg";

            if (!new File(coverPath).exists()) new File(coverPath).getParentFile().mkdirs();

            try {
                FileOutputStream out = new FileOutputStream(coverPath);

                cover.compress(Bitmap.CompressFormat.JPEG, 100, out);

                return true;
            } catch (IOException e) {
                Log.e("IOException", " = " + e.getMessage());

                return false;
            }
        }

        public String getPagePath(int number) {
            if (!book.extracted || number > getSpineList().size()) return null;

            return book.extractLocation + File.separator + getSpineList().get(number - 1);
        }

        private Document getMetaForSaving() throws Exception {
            if (root != null && metaData != null) {
                Element eCover = metaDocument.createElement("meta");
                eCover.setAttribute("name", "cover");
                eCover.setAttribute("content", "cover");
                metaData.appendChild(eCover);

                Element spine = metaDocument.createElement("spine");
                spine.setAttribute("tox", "ncx");
                root.appendChild(spine);

                Element manifest = metaDocument.createElement("manifest");
                root.appendChild(manifest);

                Element mCover = metaDocument.createElement("item");
                mCover.setAttribute("href", new File(getCoverPath()).getName());
                mCover.setAttribute("id", "cover");
                mCover.setAttribute("media-type", "image/jpeg");
                manifest.appendChild(mCover);

                for (BookMeta.ContentsItem c : getTableOfContents()) {
                    Element spineItem = metaDocument.createElement("itemref");
                    spineItem.setAttribute("idref", c.name);
                    spine.appendChild(spineItem);

                    Element mItem = metaDocument.createElement("item");
                    mItem.setAttribute("href", "pages/" + c.name + ".html");
                    mItem.setAttribute("id", c.name);
                    mItem.setAttribute("media-type", "application/xhtml+xml");
                    manifest.appendChild(mItem);
                }

                return metaDocument;
            }

            return null;
        }

        public void save() throws InvalidBookException {
            book.saveXML(metaDocument, book.contentPath);
        }

        public void list() {
            NodeList metadata;

            if (metaDocument.getElementsByTagName("metadata").getLength() == 0)
                metadata = metaDocument.getElementsByTagName("opf:metadata").item(0).getChildNodes();
            else metadata = metaDocument.getElementsByTagName("metadata").item(0).getChildNodes();

            for (int i = 0; i < metadata.getLength(); i++) {
                Node node = metadata.item(i);

                Log.i(node.getNodeName(), node.getTextContent());
            }
        }

        public static class ContentsItem {

            private String path, name, cClass;

            public ContentsItem(String path, String name) {
                this.path = path;
                this.name = name;
                this.cClass = "chapter";
            }

            public String getPath() {
                return path;
            }

            public String getExtension() {
                int i = path.lastIndexOf('.');

                return path.substring(i);
            }

            public String getName() {
                return name;
            }

        }


    }

}
