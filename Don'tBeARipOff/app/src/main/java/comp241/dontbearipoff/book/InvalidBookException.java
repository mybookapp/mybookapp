package comp241.dontbearipoff.book;

public class InvalidBookException extends Exception {

    public InvalidBookException() {
        super("Book is invalid");
    }

    public InvalidBookException(String message) {
        super(message);
    }

}
