package comp241.dontbearipoff;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import comp241.dontbearipoff.book.Book;

public class MainFragment extends ImageGallery {

    private static final String TAG = "MainActivity";

    private ArrayList<BookItem> books = new ArrayList<>();
    private Category category;

    private ArrayList<MenuItem> menuItems = new ArrayList<>();

    public MainFragment() {
        layoutId = R.layout.fragment_main;
        gridViewId = R.id.gridView;
        gridLayoutId = R.layout.item_gallery;
        toHideId = R.id.loadingPanel;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().invalidateOptionsMenu();
        setHasOptionsMenu(true);

        update();
    }

    @Override
    public void update() {
        books = Gallery.getByCategory(category);

        updateMenus();
        super.update();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.fragment_main, menu);

        menuItems.clear();
        menuItems.add(menu.findItem(R.id.action_sort_all));
        menuItems.add(menu.findItem(R.id.action_sort_book));
        menuItems.add(menu.findItem(R.id.action_sort_comic));
        menuItems.add(menu.findItem(R.id.action_sort_recipe));
        menuItems.add(menu.findItem(R.id.action_sort_other));

        updateMenus();

        super.onCreateOptionsMenu(menu, inflater);
    }

    private void updateMenus() {
        for (int i = 0; i < menuItems.size(); i ++) {
            if (i == 0) menuItems.get(i).setTitle("Show All (" + Gallery.books.size() + ")");
            else menuItems.get(i).setTitle("Show " + Category.values()[i - 1].getName() + " (" + Gallery.getByCategory(Category.values()[i - 1]).size() + ")");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        switch (id) {
            case R.id.action_sort_all: {
                category = null;

                item.setChecked(!item.isChecked());
                update();
            } return true;
            case R.id.action_sort_book: {
                category = Category.BOOK;

                item.setChecked(!item.isChecked());
                update();
            } return true;
            case R.id.action_sort_comic: {
                category = Category.COMIC;

                item.setChecked(!item.isChecked());
                update();
            } return true;
            case R.id.action_sort_recipe: {
                category = Category.RECIPE;

                item.setChecked(!item.isChecked());
                update();
            } return true;
            case R.id.action_sort_other: {
                category = Category.OTHER;

                item.setChecked(!item.isChecked());
                update();
            } return true;
            case R.id.action_new_book: {
                Intent intent = new Intent(getActivity(), NewBookActivity.class);
                startActivity(intent);
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected ArrayList<BookItem> getItems() {
        return books;
    }

    @Override
    protected ViewHolder newInstance(View view) {
        return new BookViewHolder(this, (ImageView) view.findViewById(R.id.image), (TextView) view.findViewById(R.id.text));
    }

    public static class BookViewHolder extends ViewHolder {

        private MainFragment context;

        private ImageView imageView;
        private TextView textView;

        public BookViewHolder(MainFragment context, ImageView imageView, TextView textView) {
            this.context = context;
            this.imageView = imageView;
            this.textView = textView;
        }

        @Override
        protected void update(int position, View parent) {
            BookItem book = context.getItems().get(position);

            context.loadBitmap(book.coverLocation, imageView, parent);
            textView.setText(book.meta.getTitle());
        }

    }

    public static class BookItem {

        private comp241.dontbearipoff.book.Book book;
        private Book.BookMeta meta;
        private String coverLocation;
        private String bookLocation;

        public BookItem(comp241.dontbearipoff.book.Book book, Book.BookMeta meta, String coverLocation, String bookLocation) {
            this.book = book;
            this.meta = meta;
            this.coverLocation = coverLocation;
            this.bookLocation = bookLocation;
        }

        public Book getBook() {
            return book;
        }

        public Book.BookMeta getMeta() {
            return meta;
        }

        public String getCoverLocation() {
            return coverLocation;
        }

        public String getBookLocation() {
            return bookLocation;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, final int position, long id) {
        new AlertDialog.Builder(getActivity()).setTitle("Select Book Type").setMessage("Which book would you like to open?").setPositiveButton("Photo Book", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getActivity(), BookViewActivity.class);

                intent.putExtra("BookName", getItems().get(position).meta.getTitle() + ".iepub");
                startActivity(intent);
            }
        }).setNegativeButton("Experimental OCR", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(getActivity(), BookViewActivity.class);

                intent.putExtra("BookName", getItems().get(position).meta.getTitle() + ".epub");
                startActivity(intent);
            }
        }).create().show();
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        ((MainActivity) getActivity()).openDetails(getItems().get(position));

        return true;
    }

}