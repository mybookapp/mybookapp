package comp241.dontbearipoff;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import comp241.dontbearipoff.book.Book;

public class LoadingActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_loading);

        ((TextView) findViewById(R.id.loadingTitle)).setText(getIntent().getStringExtra("title"));
        ((TextView) findViewById(R.id.loadingSubtitle)).setText(getIntent().getStringExtra("subtitle"));

        switch (getIntent().getIntExtra("taskType", 1)) {
            case 1: {
                new LoadingTask().execute();
            }
            break;
            case 2: {
                new BookLoadingTask().execute();
            }
            break;
        }
    }

    private class LoadingTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            Storage.copyAssetsFolder("tessdata");

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            setResult(RESULT_OK);
            finish();
        }

    }

    private class BookLoadingTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected Void doInBackground(Void... params) {
            if (Storage.getFilesDir("books") == null) return null;

            File cacheDir = getCacheDir();
            ArrayList<MainFragment.BookItem> books = new ArrayList<>();

            for (File file : Storage.getFilesDir("books").listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File dir, String filename) {
                    return filename.endsWith(".epub");
                }
            })) {
                comp241.dontbearipoff.book.Book book = new Book(file.getPath());

                try {
                    Book.BookMeta meta = book.getMeta();

                    if (book.isValid()) {
                        String title = meta.getTitle();
                        String coverPath = meta.getCoverPath();

                        if (title != null && coverPath != null) {
                            File coverFile = new File(cacheDir, title + ".epub");

                            if (!coverFile.exists()) {
                                InputStream inputStream = book.getFile(coverPath);
                                OutputStream outputStream = new FileOutputStream(coverFile);

                                int read = 0;
                                byte[] bytes = new byte[1024];

                                while ((read = inputStream.read(bytes)) != -1) {
                                    outputStream.write(bytes, 0, read);
                                }

                                inputStream.close();
                                outputStream.close();
                            }

                            if (coverFile.exists())
                                books.add(new MainFragment.BookItem(book, meta, coverFile.getPath(), file.getPath()));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            Gallery.books = books;

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            setResult(RESULT_OK);
            finish();
        }

    }

}
