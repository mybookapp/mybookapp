package comp241.dontbearipoff;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;

import comp241.dontbearipoff.camera.CameraType;
import comp241.dontbearipoff.camera.CameraUtils;
import comp241.dontbearipoff.camera.ICamera;
import comp241.dontbearipoff.camera.LegacyCamera;

public class NewPageActivity extends ActionBarActivity {
    private static final String TAG = "BookApp/NewPage";

    private ICamera mCamera;
    private FocusSurface mSurfaceView;

    private File mImage;

    private boolean takenPicture = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_page);

        // no camera on device
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            Log.e(TAG, "Device has no camera!");
            Toast.makeText(this, "No back facing camera found.", Toast.LENGTH_LONG).show();
            finish();
        }

        openCamera();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (mCamera.isHeld()) {
            mCamera.release();

            FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
            preview.removeView(mCamera.getView());
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (!mCamera.isHeld() && mSurfaceView == null) {
            openCamera();
        }
    }

    public void doCapture(View view) {
        if (!takenPicture) {
            if (!mCamera.isHeld()) return;

            if (mCamera.getType() == CameraType.LEGACY) {
                mCamera.capture(new PhotoHandler(this)); // TODO: allow multiple photos per page
                // TODO: Should we allow multiple photos per page?
            } else {
                // TODO: figure out how to retrieve file path after capturing with camera2
            }
        } else {
            finishActivityWithResult();
        }
    }

    public void doFocus(View view) {
        mCamera.focus();
    }

    private void finishActivityWithResult() {
        Bundle conData = new Bundle();
        conData.putString("file_path", mImage.getPath());
        conData.putParcelable("focus", mSurfaceView.getSelection());
        conData.putParcelable("dimensions", mSurfaceView.getDimensions());
        Intent intent = new Intent();
        intent.putExtras(conData);
        setResult(RESULT_OK, intent);
        finish();
    }

    private void openCamera() {
        if (mCamera == null) {
            // TODO: uncomment this to test the camera2 wrapper
            /*if (Build.VERSION.SDK_INT >= 21)
                mCamera = new ModernCamera(this);
            else*/
            mCamera = new LegacyCamera(this);
        }

        FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
        preview.removeAllViews();

        if (!mCamera.isHeld()) {
            mCamera.open();

            preview.addView(mCamera.getView());
        }

        if (mSurfaceView == null) {
            mSurfaceView = new FocusSurface(this);
            preview.addView(mSurfaceView);
        }
    }

    // TODO: These orientations work for my phone as described in the log outputs, would be great to test on other devices
    private int detectOrientation() {
        int angle;
        Display display = getWindowManager().getDefaultDisplay();
        switch (display.getRotation()) {
            case Surface.ROTATION_0: // This is display orientation
                return 90; // This is camera orientation
            case Surface.ROTATION_90:
                return 0;
            case Surface.ROTATION_180:
                return 270;
            case Surface.ROTATION_270:
                return 180;
            default:
                return 90;
        }
    }

    private void triggerFocus() {
        FrameLayout preview = (FrameLayout)findViewById(R.id.camera_preview);

        int width = preview.getWidth();
        int height = preview.getHeight();

        mCamera.getView().setVisibility(View.GONE);
        findViewById(R.id.button_focus).setVisibility(View.INVISIBLE);

        ((Button)findViewById(R.id.button_capture)).setText(R.string.action_confirm);

        Bitmap bmp = BitmapUtils.decodeSampledBitmapFromFile(mImage.getAbsolutePath(), width, height);
        mSurfaceView.setBitmap(bmp);
    }

    private class PhotoHandler implements Camera.PictureCallback {
        private static final String TAG = "PhotoHandler";
        private Context mContext;

        public PhotoHandler(Context ctx) {
            mContext = ctx;
        }

        @Override
        public void onPictureTaken(byte[] data, Camera camera) {
            String filename = CameraUtils.makePhotoPath();
            File pictureFile = new File(filename);

            Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length);

            Matrix matrix = new Matrix();
            matrix.postRotate(detectOrientation());

            bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true);

            try {
                FileOutputStream fos = new FileOutputStream(pictureFile);
                bmp.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                mImage = pictureFile;
                takenPicture = true;

                triggerFocus();
            } catch (Exception error) {
                Log.d(TAG, "File" + filename + " not saved: "
                        + error.getMessage());
            }
        }
    }
}