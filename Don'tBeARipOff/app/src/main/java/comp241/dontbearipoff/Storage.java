package comp241.dontbearipoff;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;

/*
Universal interface for external and internal storage mediums.
 */
public abstract class Storage {
    private static final String TAG = "BookApp/Storage";
    private static Context context;

    public static boolean exists(String type, String file) {
        File test = new File(getFilesDir(type) + File.separator + file);
        return test.exists();
    }

    public static boolean hasExternalStorage() {
        String state = Environment.getExternalStorageState();

        return Environment.MEDIA_MOUNTED.equals(state);
    }

    public static File getFilesDir(String type) {
        if (context == null) {
            Log.e(TAG, "No context set!");
            return null;
        }

        if (hasExternalStorage()) {
            return context.getExternalFilesDir(type);
        } else {
            return context.getFilesDir();
        }
    }

    public static boolean hasFiles(String type) {
        return exists(null, type) && getFilesDir(type) != null && getFilesDir(type).list() != null && getFilesDir(type).list().length > 0;
    }

    public static File[] listFiles(String type) {
        ArrayList<File> ret = new ArrayList<>();

        if (hasExternalStorage()) {
            ret.addAll(Arrays.asList(context.getExternalFilesDir(type).listFiles()));
        }

        ret.addAll(Arrays.asList(context.getFilesDir().listFiles()));

        return ret.toArray(new File[ret.size()]);
    }

    public static void setContext(Context ctx) {
        context = ctx;
    }

    // Thanks http://stackoverflow.com/questions/15574983/copy-directory-from-assets-to-local-directory/15575813#15575813
    public static void copyAssetsFolder(String name) {
        // "Name" is the name of your folder!
        AssetManager assetManager = context.getAssets();
        String[] files = null;

        String state = Environment.getExternalStorageState();

        String target = getFilesDir(null).getPath() + File.separator;

        if (Environment.MEDIA_MOUNTED.equals(state)) {
            // We can read and write the media
            // Checking file on assets subfolder
            try {
                files = assetManager.list(name);
            } catch (IOException e) {
                Log.e("ERROR", "Failed to get asset file list.", e);
            }

            // Analyzing all file on assets subfolder
            for (String filename : files) {
                InputStream in = null;
                OutputStream out = null;
                // First: checking if there is already a target folder

                File folder = new File(target + name);
                boolean success = true;
                if (!folder.exists()) {
                    success = folder.mkdir();
                }
                if (success) {
                    // Moving all the files on external SD
                    try {
                        in = assetManager.open(name + "/" + filename);
                        out = new FileOutputStream(target + name + "/" + filename);
                        Log.i("WEBVIEW", target + name + "/" + filename);
                        copyFile(in, out);
                        in.close();
                        in = null;
                        out.flush();
                        out.close();
                        out = null;
                    } catch (IOException e) {
                        Log.e("ERROR", "Failed to copy asset file: " + filename, e);
                    } finally {
                        // Edit 3 (after MMs comment)
                        //in.close();
                        //in = null;
                        //out.flush();
                        //out.close();
                        //out = null;
                    }
                } else {
                    // Do something else on failure
                }
            }
        } else if (Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            // We can only read the media
        } else {
            // Something else is wrong. It may be one of many other states, but all we need
            // is to know is we can neither read nor write
        }
    }

    // Method used by copyAssets() on purpose to copy a file.
    private static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }
    }


    public static void clearDirectory(File toClear) {
        File[] files = toClear.listFiles();

        for (File file : files) {
            if (file.isDirectory()) {
                Storage.clearDirectory(file);
                file.delete();
                continue;
            }
            file.delete();
        }

    }

    public static void clearDirectory(String toClear) {
        File clear = new File(toClear);
        if (clear.exists()) {
            if (!clear.isDirectory()) {
                return;
            }
            File[] files = clear.listFiles();

            for (File file : files) {
                if (file.isDirectory()) {
                    Storage.clearDirectory(file);
                    file.delete();
                    continue;
                }
                file.delete();
            }
        }
    }


}
