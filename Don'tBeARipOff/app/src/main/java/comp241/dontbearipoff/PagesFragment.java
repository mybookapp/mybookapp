package comp241.dontbearipoff;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;

import java.util.ArrayList;

public class PagesFragment extends ImageGallery {

    public PagesFragment() {
        layoutId = R.layout.fragment_pages;
        gridViewId = R.id.gridView;
        gridLayoutId = R.layout.item_gallery;
        toHideId = R.id.loadingPanel;
    }

    @Override
    protected ArrayList<ImageItem> getItems() {
        return ((PageEditActivity) getActivity()).getPages();
    }

    @Override
    protected ViewHolder newInstance(View view) {
        view.findViewById(R.id.text).setVisibility(View.GONE);

        return new ImageViewHolder(this, (ImageView) view.findViewById(R.id.image));
    }

    public static class ImageViewHolder extends ViewHolder {

        private PagesFragment context;

        private ImageView imageView;

        public ImageViewHolder(PagesFragment context, ImageView imageView) {
            this.context = context;
            this.imageView = imageView;
        }

        @Override
        protected void update(int position, View parent) {
            context.loadBitmap(context.getItems().get(position).imageLocation, imageView, parent);
        }

    }

    public static class ImageItem {

        private String imageLocation;

        public ImageItem(String imageLocation) {
            this.imageLocation = imageLocation;
        }

        public String getImageLocation() {
            return imageLocation;
        }

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // TODO: PageRegionActivity
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        return false;
    }

}
