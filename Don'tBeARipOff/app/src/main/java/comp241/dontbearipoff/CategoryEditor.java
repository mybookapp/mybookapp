package comp241.dontbearipoff;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.HashMap;

public class CategoryEditor implements View.OnClickListener {

    private static CategoryEditor instance;

    private Context context;
    private CategoryCallback callback;

    private HashMap<Category, View> categories;

    public CategoryEditor(Context context, CategoryCallback callback) {
        this.context = context;
        this.callback = callback;
        this.categories = new HashMap<>();
    }

    public void init(LayoutInflater inflater, ViewGroup parent) {
        for (Category category : Category.values()) {
            View view = inflater.inflate(R.layout.category_item, parent, false);

            ((EditText) view.findViewById(R.id.categoryName)).setText(category.getName());
            categories.put(category, view);
            parent.addView(view);
            view.findViewById(R.id.add).setOnClickListener(this);
            view.findViewById(R.id.add).setTag(category.getId());
            view.findViewById(R.id.remove).setOnClickListener(this);
            view.findViewById(R.id.remove).setTag(category.getId());
        }
    }

    public void setVisible(boolean visible) {
        for (Category category : categories.keySet()) {
            View view = categories.get(category);

            try {
                if (callback.hasCategory(category)) {
                    view.findViewById(R.id.add).setVisibility(View.GONE);
                    view.findViewById(R.id.remove).setVisibility(visible ? View.VISIBLE : View.GONE);
                } else {
                    view.findViewById(R.id.add).setVisibility(visible ? View.VISIBLE : View.GONE);
                    view.findViewById(R.id.remove).setVisibility(View.GONE);
                }
            } catch (Exception e) {
                view.findViewById(R.id.add).setVisibility(visible ? View.VISIBLE : View.GONE);
                view.findViewById(R.id.remove).setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void onClick(View view) {
        String categoryId = (String) view.getTag();
        Category category = Category.getById(categoryId);
        View categoryView = categories.get(category);

        if (view.getId() == R.id.add) {
            categoryView.findViewById(R.id.add).setVisibility(View.GONE);
            categoryView.findViewById(R.id.remove).setVisibility(View.VISIBLE);
            callback.onAddCategory(category);
        } else {
            categoryView.findViewById(R.id.add).setVisibility(View.VISIBLE);
            categoryView.findViewById(R.id.remove).setVisibility(View.GONE);
            callback.onRemoveCategory(category);
        }
    }

    public interface CategoryCallback {

        boolean hasCategory(Category category);

        void onAddCategory(Category category);

        void onRemoveCategory(Category category);

    }

}
