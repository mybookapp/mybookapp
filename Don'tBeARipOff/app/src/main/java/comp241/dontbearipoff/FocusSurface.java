package comp241.dontbearipoff;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.Size;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

public class FocusSurface extends SurfaceView implements SurfaceHolder.Callback {
    private Bitmap mBitmap;
    private FocusBoxView mFocusBoxView;

    private final Paint mPaint;
    private final int mMaskColor;
    private final int mFrameColor;
    private final int mCornerColor;
    private final int mLockedCornerColor;

    private final Rect mCanvasBounds = new Rect();

    private static final String TAG = "BookApp/FocusSurface";

    public FocusSurface(Context context) {
        super(context);
        getHolder().addCallback(this);

        mFocusBoxView = new FocusBoxView(getContext(), this);

        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);

        Resources resources = getResources();
        mMaskColor = resources.getColor(R.color.focus_box_mask);
        mFrameColor = resources.getColor(R.color.focus_box_frame);
        mCornerColor = resources.getColor(R.color.focus_box_corner);
        mLockedCornerColor = resources.getColor(R.color.focus_box_corner_locked);

        setVisibility(GONE);
        setWillNotDraw(true);
    }

    public Rect getDimensions() {
        return mCanvasBounds;
    }

    public Rect getSelection() {
        return mFocusBoxView.getSelection();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawColor(Color.RED);
        canvas.getClipBounds(mCanvasBounds);

        if (!mFocusBoxView.hasConstraints())
            mFocusBoxView.setConstraints(mCanvasBounds);

        if (mBitmap == null) return;

        //canvas.drawBitmap(mBitmap, 0, 0, new Paint());
        mPaint.setAlpha(255);
        canvas.drawBitmap(mBitmap, null, mCanvasBounds, null);

        //Log.i(TAG, "Drawing focus box.");
        Rect rect = mFocusBoxView.getSelection();
        //Log.i(TAG, "Surface View: " + this.getWidth() + "x" + this.getHeight());
        //Log.i(TAG, "Selection: (" + rect.left + ", " + rect.top + "), (" + rect.right + ", " + rect.bottom + ")");

        final int CORNER_SIZE = FocusBoxView.CORNER_SIZE;
        int c = mFocusBoxView.getLockedCorner();

        drawOutlinedRect(canvas, mMaskColor, rect.left, rect.top, rect.right, rect.bottom);

        drawOutlinedRect(canvas, (c == FocusBoxView.TOP_LEFT ? mLockedCornerColor : mCornerColor), rect.left - CORNER_SIZE, rect.top - CORNER_SIZE, rect.left + CORNER_SIZE, rect.top + CORNER_SIZE);
        drawOutlinedRect(canvas, (c == FocusBoxView.TOP_RIGHT ? mLockedCornerColor : mCornerColor), rect.right - CORNER_SIZE, rect.top - CORNER_SIZE, rect.right + CORNER_SIZE, rect.top + CORNER_SIZE);
        drawOutlinedRect(canvas, (c == FocusBoxView.BOTTOM_LEFT ? mLockedCornerColor : mCornerColor), rect.left - CORNER_SIZE, rect.bottom - CORNER_SIZE, rect.left + CORNER_SIZE, rect.bottom + CORNER_SIZE);
        drawOutlinedRect(canvas, (c == FocusBoxView.BOTTOM_RIGHT ? mLockedCornerColor : mCornerColor), rect.right - CORNER_SIZE, rect.bottom - CORNER_SIZE, rect.right + CORNER_SIZE, rect.bottom + CORNER_SIZE);
    }

    private void drawOutlinedRect(Canvas canvas, int color, int x1, int y1, int x2, int y2) {
        Paint.Style temp = mPaint.getStyle();

        mPaint.setColor(color);
        mPaint.setStyle(Paint.Style.FILL);
        canvas.drawRect(x1, y1, x2, y2, mPaint);

        mPaint.setColor(mFrameColor);
        mPaint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(x1, y1, x2, y2, mPaint);

        mPaint.setStyle(temp);
    }

    public void setBitmap(Bitmap bmp) {
        mBitmap = bmp;

        setVisibility(VISIBLE);
        setWillNotDraw(false);
        setOnTouchListener(mFocusBoxView.getTouchListener());
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Canvas canvas = null;
        try {
            canvas = holder.lockCanvas();
            synchronized (holder) {
                //onDraw(canvas);
                draw(canvas);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (canvas != null) {
                holder.unlockCanvasAndPost(canvas);
            }
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }
}