package comp241.dontbearipoff;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * @param <I> Item type
 * @param <H> Holder type
 */
public abstract class ImageGallery<I, H extends ImageGallery.ViewHolder> extends Fragment implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener {

    protected View view;

    protected int layoutId, gridViewId;
    protected int gridLayoutId, toHideId = -1;

    protected ImageAdapter adapter;

    public ImageGallery() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        adapter = new ImageAdapter(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(layoutId, container, false);
        GridView gridView = (GridView) view.findViewById(gridViewId);

        gridView.setAdapter(adapter);
        gridView.setOnItemClickListener(this);
        gridView.setOnItemLongClickListener(this);

        return view;
    }

    public void update() {
        if (adapter != null) adapter.notifyDataSetChanged();
    }

    private class ImageAdapter extends BaseAdapter {

        private final Context context;

        public ImageAdapter(Context context) {
            super();

            this.context = context;
        }

        @Override
        public int getCount() {
            return getItems().size();
        }

        @Override
        public Object getItem(int position) {
            return getItems().get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                LayoutInflater inflater = ((Activity) context).getLayoutInflater();
                convertView = inflater.inflate(gridLayoutId, parent, false);

                holder = newInstance(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.update(position, convertView);

            return convertView;
        }

    }

    protected abstract ArrayList<I> getItems();

    protected abstract ViewHolder newInstance(View view);

    protected static abstract class ViewHolder {

        protected abstract void update(int position, View parent);

    }

    public void loadBitmap(String location, ImageView imageView, View parent) {
        if (cancelPotentialWork(location, imageView)) {
            final BitmapWorkerTask task = toHideId != -1 ? new BitmapWorkerTask(imageView, parent.findViewById(toHideId)) : new BitmapWorkerTask(imageView);
            final AsyncDrawable asyncDrawable = new AsyncDrawable(getResources(), null, task);

            imageView.setImageDrawable(asyncDrawable);
            task.execute(location);
        }
    }

    static class AsyncDrawable extends BitmapDrawable {

        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap, BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);

            bitmapWorkerTaskReference = new WeakReference<>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }

    }

    public static boolean cancelPotentialWork(String data, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final String bitmapData = bitmapWorkerTask.getLocation();

            if (bitmapData != null && !bitmapData.equals(data)) bitmapWorkerTask.cancel(true);
            else return false;
        }

        return true;
    }

    private static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();

            if (drawable instanceof AsyncDrawable) {
                final AsyncDrawable asyncDrawable = (AsyncDrawable) drawable;

                return asyncDrawable.getBitmapWorkerTask();
            }
        }

        return null;
    }

}
