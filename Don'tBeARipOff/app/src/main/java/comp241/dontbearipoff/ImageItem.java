package comp241.dontbearipoff;

import android.graphics.Bitmap;

public class ImageItem {
    private Bitmap image;
    private String text;

    public ImageItem(Bitmap image) {
        super();
        this.image = image;

    }

    public ImageItem(Bitmap image, String text) {
        super();
        this.image = image;
        this.text = text;
    }

    public Bitmap getImage() {
        return image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }
}