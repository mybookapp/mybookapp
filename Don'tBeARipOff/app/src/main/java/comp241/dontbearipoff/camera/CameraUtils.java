package comp241.dontbearipoff.camera;

import android.os.Environment;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import comp241.dontbearipoff.Storage;

public abstract class CameraUtils {
    public static String makePhotoPath() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd-HHmmss");
        String date = dateFormat.format(new Date());

        return Storage.getFilesDir(Environment.DIRECTORY_PICTURES) + File.separator + "Picture_" + date + ".jpg";
    }
}
