package comp241.dontbearipoff.camera;

public enum CameraType {
    LEGACY, MODERN
}