package comp241.dontbearipoff.camera;

import android.app.Activity;
import android.content.Context;
import android.hardware.Camera;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.IOException;

public class LegacyCamera extends SurfaceView implements ICamera {
    private Camera mCamera;
    private Context mContext;
    private SurfaceHolder mHolder;

    private Camera.AutoFocusCallback mAutoFocusCallback = new Camera.AutoFocusCallback() {
        @Override
        public void onAutoFocus(boolean success, Camera camera) {

        }
    };

    public static final String TAG = "BookApp/LegacyCamera";

    public LegacyCamera(Context context) {
        super(context);

        mCamera = null;
        mContext = context;
        mHolder = getHolder();
        mHolder.addCallback(this);
    }

    @Override
    public void capture(Object arg) {
        Camera.PictureCallback callback = (Camera.PictureCallback) arg;

        mCamera.takePicture(null, null, callback);
    }

    @Override
    public void focus() {
        mCamera.autoFocus(mAutoFocusCallback);
    }

    public CameraType getType() {
        return CameraType.LEGACY;
    }

    public View getView() {
        return this;
    }

    @Override
    public boolean isHeld() {
        return mCamera != null;
    }

    public void open() {
        if (!isHeld()) mCamera = getCameraInstance();
    }

    public void release() {
        if (isHeld()) {
            mCamera.release();
            mCamera = null;
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        if (mHolder.getSurface() == null) return;

        try {
            mCamera.stopPreview();
        } catch (Exception e) {
            // ignore
        }

        surfaceCreated(mHolder);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        try {
            detectOrientation();
            mCamera.setPreviewDisplay(mHolder);
            mCamera.startPreview();
        } catch (IOException e) {
            Log.d(TAG, "Error setting up camera preview: " + e.getMessage());
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {

    }

    private void detectOrientation() {
        int angle;
        Display display = ((Activity) mContext).getWindowManager().getDefaultDisplay();

        switch (display.getRotation()) {
            case Surface.ROTATION_0: // This is display orientation
                angle = 90; // This is camera orientation
                break;
            case Surface.ROTATION_90:
                angle = 0;
                break;
            case Surface.ROTATION_180:
                angle = 270;
                break;
            case Surface.ROTATION_270:
                angle = 180;
                break;
            default:
                angle = 90;
                break;
        }

        Log.v(TAG, "angle: " + angle);
        mCamera.setDisplayOrientation(angle);
    }

    private Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
            e.printStackTrace();
            // Camera is not available
        }
        return c;
    }
}