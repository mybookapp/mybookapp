package comp241.dontbearipoff.camera;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.graphics.ImageFormat;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Handler;
import android.os.HandlerThread;
import android.util.Log;
import android.util.Size;
import android.view.Surface;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@TargetApi(21)
public class ModernCamera extends SurfaceView implements ICamera {
    public static final String TAG = "BookApp/ModernCamera";

    private Context mContext;
    private SurfaceHolder mHolder;

    private HandlerThread mBackgroundThread;
    private Handler mBackgroundHandler;
    private Handler mForegroundHandler;
    private CameraManager mCameraManager;

    private CameraDevice mCamera;
    private String mCameraId;
    private boolean mGotSecondCallback;

    private ImageReader mCaptureBuffer;
    private CameraCaptureSession mCaptureSession;
    private CameraCaptureSession.StateCallback mCaptureSessionListener = new CameraCaptureSession.StateCallback() {
        @Override
        public void onConfigured(CameraCaptureSession session) {
            mCaptureSession = session;

            if (mHolder != null) {
                try {
                    // Build a request for preview footage
                    CaptureRequest.Builder requestBuilder =
                            mCamera.createCaptureRequest(mCamera.TEMPLATE_PREVIEW);
                    requestBuilder.addTarget(mHolder.getSurface());
                    CaptureRequest previewRequest = requestBuilder.build();
                    // Start displaying preview images
                    try {
                        session.setRepeatingRequest(previewRequest, null, null);
                    } catch (CameraAccessException ex) {
                        Log.e(TAG, "Failed to make repeating preview request", ex);
                    }
                } catch (CameraAccessException ex) {
                    Log.e(TAG, "Failed to build preview request", ex);
                }
            } else {
                Log.e(TAG, "Holder didn't exist when trying to formulate preview request");
            }
        }

        @Override
        public void onClosed(CameraCaptureSession session) {
            mCaptureSession = null;
        }

        @Override
        public void onConfigureFailed(CameraCaptureSession session) {
            Log.e(TAG, "Configuration error on device: " + mCamera.getId());
        }
    };

    private CameraDevice.StateCallback mCameraStateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice device) {
            mCamera = device;

            try {
                List<Surface> outputs = Arrays.asList(mHolder.getSurface(), mCaptureBuffer.getSurface());
                device.createCaptureSession(outputs, mCaptureSessionListener, mBackgroundHandler);
            } catch (CameraAccessException e) {
                Log.e(TAG, "Failed to create a capture session.", e);
            }
        }

        @Override
        public void onDisconnected(CameraDevice device) {
        }

        @Override
        public void onError(CameraDevice device, int error) {
            Log.e(TAG, "Camera (" + device.getId() + ") error occurred: " + error);
        }
    };

    public ModernCamera(Context context) {
        super(context);
        mContext = context;
        mHolder = getHolder();
    }

    @Override
    public void capture(Object callback) {
        // onClickOnSurfaceView
    }

    @Override
    public void focus() {

    }

    @Override
    public CameraType getType() {
        return CameraType.MODERN;
    }

    @Override
    public View getView() {
        return this;
    }

    @Override
    public boolean isHeld() {
        return false;
    }

    @Override
    public void open() {
        mBackgroundThread = new HandlerThread("background");
        mBackgroundThread.start();
        mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
        mForegroundHandler = new Handler(mContext.getMainLooper());

        mCameraManager = (CameraManager) mContext.getSystemService(Activity.CAMERA_SERVICE);
        mHolder.addCallback(this);
    }

    @Override
    public void release() {
        try {
            mHolder.setFixedSize(0, 0);

            if (mCaptureSession != null) {
                mCaptureSession.close();
                mCaptureSession = null;
            }
        } finally {
            if (mCamera != null) {
                mCamera.close();
                mCamera = null;
            }
        }

        mBackgroundThread.quitSafely();
        try {
            mBackgroundThread.join();
        } catch (InterruptedException e) {
            Log.e(TAG, "Background worker thread was interrupted while joined", e);
        }

        if (mCaptureBuffer != null) mCaptureBuffer.close();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // first invocation
        if (mCameraId == null) {
            try {
                for (String cameraId : mCameraManager.getCameraIdList()) {
                    CameraCharacteristics characteristics = mCameraManager.getCameraCharacteristics(cameraId);
                    if (characteristics.get(characteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_BACK) {
                        StreamConfigurationMap info = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);

                        // find largest supported resolution
                        Size largest = Collections.max(Arrays.asList(info.getOutputSizes(ImageFormat.JPEG)), new CompareByArea());

                        Log.i(TAG, "Chosen capture size: " + largest);

                        mCaptureBuffer = ImageReader.newInstance(largest.getWidth(), largest.getHeight(), ImageFormat.JPEG, 2);
                        mCaptureBuffer.setOnImageAvailableListener(mImageCaptureListener, mBackgroundHandler);

                        Size optimalSize = chooseBigEnoughSize(info.getOutputSizes(SurfaceHolder.class), width, height);

                        mHolder.setFixedSize(optimalSize.getWidth(), optimalSize.getHeight());
                        mCameraId = cameraId;

                        return;
                    }
                }
            } catch (CameraAccessException e) {
                Log.e(TAG, "Unable to list cameras", e);
            }

            Log.e(TAG, "Didn't find any back-facing cameras.");
        } else if (!mGotSecondCallback) {
            // mHolder.setFixedSize causes this event to fire a second time
            if (mCamera != null) {
                Log.e(TAG, "Attempting to open camera when it is already open.");
                return;
            }

            try {
                mCameraManager.openCamera(mCameraId, mCameraStateCallback, mBackgroundHandler);
            } catch (CameraAccessException e) {
                Log.e(TAG, "Failed to configure output surface.", e);
            }

            mGotSecondCallback = true;
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        Log.i(TAG, "Surface created");
        mCameraId = null;
        mGotSecondCallback = false;
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        mHolder.removeCallback(this);
    }

    private Size chooseBigEnoughSize(Size[] choices, int width, int height) {
        List<Size> bigEnough = new ArrayList<>();
        for (Size option : choices) {
            if (option.getWidth() >= width && option.getHeight() >= height) {
                bigEnough.add(option);
            }
        }

        if (bigEnough.size() > 0) {
            return Collections.min(bigEnough, new CompareByArea());
        } else {
            Log.e(TAG, "Couldn't find any suitable preview size.");
            return choices[0];
        }
    }

    static class CompareByArea implements Comparator<Size> {
        @Override
        public int compare(Size lhs, Size rhs) {
            return Long.signum((long) lhs.getWidth() * lhs.getHeight() - (long) rhs.getWidth() * rhs.getHeight());
        }
    }

    private ImageReader.OnImageAvailableListener mImageCaptureListener = new ImageReader.OnImageAvailableListener() {
        @Override
        public void onImageAvailable(ImageReader reader) {
            mBackgroundHandler.post(new CapturedImageSaver(reader.acquireNextImage()));
        }
    };

    static class CapturedImageSaver implements Runnable {
        private Image mCapture;

        public CapturedImageSaver(Image capture) {
            mCapture = capture;
        }

        @Override
        public void run() {
            File file = new File(CameraUtils.makePhotoPath());

            try (FileOutputStream fos = new FileOutputStream(file)) {
                ByteBuffer buffer = mCapture.getPlanes()[0].getBuffer();

                byte[] jpeg = new byte[buffer.remaining()];
                buffer.get(jpeg);
                fos.write(jpeg);
            } catch (FileNotFoundException e) {
                Log.e(TAG, "Unable to open target output file for writing.", e);
            } catch (IOException e) {
                Log.e(TAG, "Failed to write the image to the output file.", e);
            }

            mCapture.close();
        }
    }
}
