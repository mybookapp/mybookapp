package comp241.dontbearipoff.camera;

import android.view.SurfaceHolder;
import android.view.View;

public interface ICamera extends SurfaceHolder.Callback {

    boolean isHeld();

    void capture(Object callback);

    void focus();

    CameraType getType();

    View getView();

    void open();

    void release();

}
