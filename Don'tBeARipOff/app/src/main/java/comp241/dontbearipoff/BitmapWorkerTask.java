package comp241.dontbearipoff;

import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ImageView;

import java.lang.ref.WeakReference;

public class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {

    private final WeakReference<ImageView> imageViewReference;
    private final WeakReference<View> loadingViewReference;

    private String location;

    public BitmapWorkerTask(ImageView imageView) {
        this(imageView, null);
    }

    public BitmapWorkerTask(ImageView imageView, View loadingView) {
        imageViewReference = new WeakReference<>(imageView);
        loadingViewReference = new WeakReference<>(loadingView);
    }

    @Override
    protected Bitmap doInBackground(String... params) {
        location = params[0];

        return BitmapUtils.decodeSampledBitmapFromFile(location, 100, 100);
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (imageViewReference.get() != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();

            if (imageView != null) {
                imageView.setImageBitmap(bitmap);

                if (loadingViewReference != null && loadingViewReference.get() != null) {
                    final View loadingView = loadingViewReference.get();

                    if (loadingView != null) loadingView.setVisibility(View.GONE);
                }

                imageView.setVisibility(View.VISIBLE);
            }
        }
    }

    public String getLocation() {
        return location;
    }

}
